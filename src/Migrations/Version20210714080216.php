<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210714080216 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE cake (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, speciality_code INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, ingredient LONGTEXT DEFAULT NULL, urlImage LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', duration INT NOT NULL, day_week VARCHAR(50) DEFAULT NULL, info_etage VARCHAR(255) DEFAULT NULL, type_patisserie INT NOT NULL, type_prix INT NOT NULL, dummy_stage INT NOT NULL, savour INT NOT NULL, supplement INT NOT NULL, topping INT NOT NULL, colour INT NOT NULL, candle INT NOT NULL, text INT NOT NULL, active INT NOT NULL, isDel INT NOT NULL, allergy VARCHAR(255) DEFAULT NULL, categorie VARCHAR(255) DEFAULT NULL, INDEX IDX_FA13015DA76ED395 (user_id), INDEX IDX_FA13015DDEDAC6BE (speciality_code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE price_cake (id INT AUTO_INCREMENT NOT NULL, cake_id INT DEFAULT NULL, size INT DEFAULT NULL, price DOUBLE PRECISION NOT NULL, quantity_min INT DEFAULT NULL, quantity_max INT DEFAULT NULL, quantity INT DEFAULT NULL, INDEX IDX_8741F3E79F8008B6 (cake_id), INDEX IDX_8741F3E7F7C0246A (size), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cake_size (id INT AUTO_INCREMENT NOT NULL, libelle INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE candle_option (id INT AUTO_INCREMENT NOT NULL, id_cake INT DEFAULT NULL, price DOUBLE PRECISION NOT NULL, max INT NOT NULL, INDEX IDX_C9912D6371331D5F (id_cake), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client_request (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, location VARCHAR(255) NOT NULL, coordinnates VARCHAR(255) NOT NULL, event VARCHAR(255) NOT NULL, number_person VARCHAR(255) NOT NULL, informations VARCHAR(255) NOT NULL, date DATETIME NOT NULL, creation_date DATETIME NOT NULL, photo LONGTEXT NOT NULL, accept TINYINT(1) NOT NULL, INDEX IDX_69AACBE2A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client_request_message (id INT AUTO_INCREMENT NOT NULL, client_request_id INT NOT NULL, user_id INT NOT NULL, user_pastry_id INT NOT NULL, creation_date DATETIME NOT NULL, message VARCHAR(3000) DEFAULT NULL, photo VARCHAR(500) DEFAULT NULL, sender VARCHAR(10) NOT NULL, INDEX IDX_E1BF1FDBD22A85E4 (client_request_id), INDEX IDX_E1BF1FDBA76ED395 (user_id), INDEX IDX_E1BF1FDB4B1D3F40 (user_pastry_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client_request_proposal (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, user_pastry_id INT NOT NULL, client_request_id INT NOT NULL, price DOUBLE PRECISION NOT NULL, creation_date DATETIME NOT NULL, accept TINYINT(1) DEFAULT NULL, INDEX IDX_6E09F1ACA76ED395 (user_id), INDEX IDX_6E09F1AC4B1D3F40 (user_pastry_id), INDEX IDX_6E09F1ACD22A85E4 (client_request_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE colour_active (id INT AUTO_INCREMENT NOT NULL, id_cake INT DEFAULT NULL, colour_list VARCHAR(255) NOT NULL, INDEX IDX_E7A98CE171331D5F (id_cake), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE colour_list (id INT AUTO_INCREMENT NOT NULL, id_user INT DEFAULT NULL, libelle VARCHAR(255) NOT NULL, hexa VARCHAR(255) NOT NULL, INDEX IDX_16B4F2D76B3CA4B (id_user), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE colour_price (id INT AUTO_INCREMENT NOT NULL, id_colour INT DEFAULT NULL, id_cake INT DEFAULT NULL, price DOUBLE PRECISION NOT NULL, INDEX IDX_C14181F2DCCFD023 (id_colour), INDEX IDX_C14181F271331D5F (id_cake), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dummy_stage_option (id INT AUTO_INCREMENT NOT NULL, id_cake INT DEFAULT NULL, price DOUBLE PRECISION NOT NULL, max INT NOT NULL, INDEX IDX_4240C35071331D5F (id_cake), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orders (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, code INT NOT NULL, validate INT NOT NULL, date_creation DATE NOT NULL, info VARCHAR(500) NOT NULL, INDEX IDX_E52FFDEEA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_cake (id INT AUTO_INCREMENT NOT NULL, cake_id INT NOT NULL, order_id INT NOT NULL, user_id INT NOT NULL, price DOUBLE PRECISION NOT NULL, total DOUBLE PRECISION NOT NULL, quantity INT NOT NULL, order_date DATE NOT NULL, dummy_stage VARCHAR(255) NOT NULL, savour VARCHAR(255) NOT NULL, supplement VARCHAR(255) NOT NULL, topping VARCHAR(255) NOT NULL, colour VARCHAR(255) NOT NULL, candle VARCHAR(255) NOT NULL, text VARCHAR(255) NOT NULL, INDEX IDX_B7E23B4A9F8008B6 (cake_id), INDEX IDX_B7E23B4A8D9F6D38 (order_id), INDEX IDX_B7E23B4AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pastry_chef_speciality (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, code INT DEFAULT NULL, etat INT NOT NULL, INDEX IDX_253FE35A76ED395 (user_id), INDEX IDX_253FE3577153098 (code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE review (id INT AUTO_INCREMENT NOT NULL, cake_id INT DEFAULT NULL, user_id INT DEFAULT NULL, nb_stars INT NOT NULL, contain VARCHAR(500) NOT NULL, INDEX IDX_794381C69F8008B6 (cake_id), INDEX IDX_794381C6A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE savour_active (id INT AUTO_INCREMENT NOT NULL, id_cake INT DEFAULT NULL, savour_list VARCHAR(255) NOT NULL, INDEX IDX_F6C6656A71331D5F (id_cake), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE savour_list (id INT AUTO_INCREMENT NOT NULL, id_user INT DEFAULT NULL, libelle VARCHAR(255) NOT NULL, INDEX IDX_9F65CE406B3CA4B (id_user), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE savour_price (id INT AUTO_INCREMENT NOT NULL, id_savour INT DEFAULT NULL, id_cake INT DEFAULT NULL, price DOUBLE PRECISION NOT NULL, INDEX IDX_AFA356295F8DDB22 (id_savour), INDEX IDX_AFA3562971331D5F (id_cake), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE speciality_list (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE supplement_active (id INT AUTO_INCREMENT NOT NULL, id_cake INT DEFAULT NULL, supplement_list VARCHAR(255) NOT NULL, INDEX IDX_AC010EB971331D5F (id_cake), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE supplement_list (id INT AUTO_INCREMENT NOT NULL, id_user INT DEFAULT NULL, libelle VARCHAR(255) NOT NULL, INDEX IDX_FC5297FC6B3CA4B (id_user), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE supplement_price (id INT AUTO_INCREMENT NOT NULL, id_supplement INT DEFAULT NULL, id_cake INT DEFAULT NULL, price DOUBLE PRECISION NOT NULL, INDEX IDX_6D179ED7A4D0F31E (id_supplement), INDEX IDX_6D179ED771331D5F (id_cake), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE text_option (id INT AUTO_INCREMENT NOT NULL, id_cake INT DEFAULT NULL, price DOUBLE PRECISION NOT NULL, INDEX IDX_4F0A994371331D5F (id_cake), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE topping_active (id INT AUTO_INCREMENT NOT NULL, id_cake INT DEFAULT NULL, topping_list VARCHAR(255) NOT NULL, INDEX IDX_9DAF812471331D5F (id_cake), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE topping_list (id INT AUTO_INCREMENT NOT NULL, id_user INT DEFAULT NULL, libelle VARCHAR(255) NOT NULL, INDEX IDX_474C94836B3CA4B (id_user), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE topping_price (id INT AUTO_INCREMENT NOT NULL, id_topping INT DEFAULT NULL, id_cake INT DEFAULT NULL, price DOUBLE PRECISION NOT NULL, INDEX IDX_AD16EC795F373D97 (id_topping), INDEX IDX_AD16EC7971331D5F (id_cake), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, type INT NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, raisonSociale VARCHAR(255) NOT NULL, firstName VARCHAR(255) NOT NULL, address VARCHAR(255) DEFAULT NULL, address2 VARCHAR(255) DEFAULT NULL, localisation VARCHAR(255) NOT NULL, city VARCHAR(255) DEFAULT NULL, zipCode VARCHAR(255) DEFAULT NULL, country VARCHAR(255) DEFAULT NULL, phone VARCHAR(30) DEFAULT NULL, email VARCHAR(255) NOT NULL, picture VARCHAR(255) NOT NULL, days TEXT NOT NULL COMMENT \'(DC2Type:array)\', aRoles TEXT NOT NULL COMMENT \'(DC2Type:array)\', active INT NOT NULL, isDel INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cake ADD CONSTRAINT FK_FA13015DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE cake ADD CONSTRAINT FK_FA13015DDEDAC6BE FOREIGN KEY (speciality_code) REFERENCES speciality_list (id)');
        $this->addSql('ALTER TABLE price_cake ADD CONSTRAINT FK_8741F3E79F8008B6 FOREIGN KEY (cake_id) REFERENCES cake (id)');
        $this->addSql('ALTER TABLE price_cake ADD CONSTRAINT FK_8741F3E7F7C0246A FOREIGN KEY (size) REFERENCES cake_size (id)');
        $this->addSql('ALTER TABLE candle_option ADD CONSTRAINT FK_C9912D6371331D5F FOREIGN KEY (id_cake) REFERENCES cake (id)');
        $this->addSql('ALTER TABLE client_request ADD CONSTRAINT FK_69AACBE2A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE client_request_message ADD CONSTRAINT FK_E1BF1FDBD22A85E4 FOREIGN KEY (client_request_id) REFERENCES client_request (id)');
        $this->addSql('ALTER TABLE client_request_message ADD CONSTRAINT FK_E1BF1FDBA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE client_request_message ADD CONSTRAINT FK_E1BF1FDB4B1D3F40 FOREIGN KEY (user_pastry_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE client_request_proposal ADD CONSTRAINT FK_6E09F1ACA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE client_request_proposal ADD CONSTRAINT FK_6E09F1AC4B1D3F40 FOREIGN KEY (user_pastry_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE client_request_proposal ADD CONSTRAINT FK_6E09F1ACD22A85E4 FOREIGN KEY (client_request_id) REFERENCES client_request (id)');
        $this->addSql('ALTER TABLE colour_active ADD CONSTRAINT FK_E7A98CE171331D5F FOREIGN KEY (id_cake) REFERENCES cake (id)');
        $this->addSql('ALTER TABLE colour_list ADD CONSTRAINT FK_16B4F2D76B3CA4B FOREIGN KEY (id_user) REFERENCES user (id)');
        $this->addSql('ALTER TABLE colour_price ADD CONSTRAINT FK_C14181F2DCCFD023 FOREIGN KEY (id_colour) REFERENCES colour_list (id)');
        $this->addSql('ALTER TABLE colour_price ADD CONSTRAINT FK_C14181F271331D5F FOREIGN KEY (id_cake) REFERENCES cake (id)');
        $this->addSql('ALTER TABLE dummy_stage_option ADD CONSTRAINT FK_4240C35071331D5F FOREIGN KEY (id_cake) REFERENCES cake (id)');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEEA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE order_cake ADD CONSTRAINT FK_B7E23B4A9F8008B6 FOREIGN KEY (cake_id) REFERENCES price_cake (id)');
        $this->addSql('ALTER TABLE order_cake ADD CONSTRAINT FK_B7E23B4A8D9F6D38 FOREIGN KEY (order_id) REFERENCES orders (id)');
        $this->addSql('ALTER TABLE order_cake ADD CONSTRAINT FK_B7E23B4AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE pastry_chef_speciality ADD CONSTRAINT FK_253FE35A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE pastry_chef_speciality ADD CONSTRAINT FK_253FE3577153098 FOREIGN KEY (code) REFERENCES speciality_list (id)');
        $this->addSql('ALTER TABLE review ADD CONSTRAINT FK_794381C69F8008B6 FOREIGN KEY (cake_id) REFERENCES cake (id)');
        $this->addSql('ALTER TABLE review ADD CONSTRAINT FK_794381C6A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE savour_active ADD CONSTRAINT FK_F6C6656A71331D5F FOREIGN KEY (id_cake) REFERENCES cake (id)');
        $this->addSql('ALTER TABLE savour_list ADD CONSTRAINT FK_9F65CE406B3CA4B FOREIGN KEY (id_user) REFERENCES user (id)');
        $this->addSql('ALTER TABLE savour_price ADD CONSTRAINT FK_AFA356295F8DDB22 FOREIGN KEY (id_savour) REFERENCES savour_list (id)');
        $this->addSql('ALTER TABLE savour_price ADD CONSTRAINT FK_AFA3562971331D5F FOREIGN KEY (id_cake) REFERENCES cake (id)');
        $this->addSql('ALTER TABLE supplement_active ADD CONSTRAINT FK_AC010EB971331D5F FOREIGN KEY (id_cake) REFERENCES cake (id)');
        $this->addSql('ALTER TABLE supplement_list ADD CONSTRAINT FK_FC5297FC6B3CA4B FOREIGN KEY (id_user) REFERENCES user (id)');
        $this->addSql('ALTER TABLE supplement_price ADD CONSTRAINT FK_6D179ED7A4D0F31E FOREIGN KEY (id_supplement) REFERENCES supplement_list (id)');
        $this->addSql('ALTER TABLE supplement_price ADD CONSTRAINT FK_6D179ED771331D5F FOREIGN KEY (id_cake) REFERENCES cake (id)');
        $this->addSql('ALTER TABLE text_option ADD CONSTRAINT FK_4F0A994371331D5F FOREIGN KEY (id_cake) REFERENCES cake (id)');
        $this->addSql('ALTER TABLE topping_active ADD CONSTRAINT FK_9DAF812471331D5F FOREIGN KEY (id_cake) REFERENCES cake (id)');
        $this->addSql('ALTER TABLE topping_list ADD CONSTRAINT FK_474C94836B3CA4B FOREIGN KEY (id_user) REFERENCES user (id)');
        $this->addSql('ALTER TABLE topping_price ADD CONSTRAINT FK_AD16EC795F373D97 FOREIGN KEY (id_topping) REFERENCES topping_list (id)');
        $this->addSql('ALTER TABLE topping_price ADD CONSTRAINT FK_AD16EC7971331D5F FOREIGN KEY (id_cake) REFERENCES cake (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE price_cake DROP FOREIGN KEY FK_8741F3E79F8008B6');
        $this->addSql('ALTER TABLE candle_option DROP FOREIGN KEY FK_C9912D6371331D5F');
        $this->addSql('ALTER TABLE colour_active DROP FOREIGN KEY FK_E7A98CE171331D5F');
        $this->addSql('ALTER TABLE colour_price DROP FOREIGN KEY FK_C14181F271331D5F');
        $this->addSql('ALTER TABLE dummy_stage_option DROP FOREIGN KEY FK_4240C35071331D5F');
        $this->addSql('ALTER TABLE review DROP FOREIGN KEY FK_794381C69F8008B6');
        $this->addSql('ALTER TABLE savour_active DROP FOREIGN KEY FK_F6C6656A71331D5F');
        $this->addSql('ALTER TABLE savour_price DROP FOREIGN KEY FK_AFA3562971331D5F');
        $this->addSql('ALTER TABLE supplement_active DROP FOREIGN KEY FK_AC010EB971331D5F');
        $this->addSql('ALTER TABLE supplement_price DROP FOREIGN KEY FK_6D179ED771331D5F');
        $this->addSql('ALTER TABLE text_option DROP FOREIGN KEY FK_4F0A994371331D5F');
        $this->addSql('ALTER TABLE topping_active DROP FOREIGN KEY FK_9DAF812471331D5F');
        $this->addSql('ALTER TABLE topping_price DROP FOREIGN KEY FK_AD16EC7971331D5F');
        $this->addSql('ALTER TABLE order_cake DROP FOREIGN KEY FK_B7E23B4A9F8008B6');
        $this->addSql('ALTER TABLE price_cake DROP FOREIGN KEY FK_8741F3E7F7C0246A');
        $this->addSql('ALTER TABLE client_request_message DROP FOREIGN KEY FK_E1BF1FDBD22A85E4');
        $this->addSql('ALTER TABLE client_request_proposal DROP FOREIGN KEY FK_6E09F1ACD22A85E4');
        $this->addSql('ALTER TABLE colour_price DROP FOREIGN KEY FK_C14181F2DCCFD023');
        $this->addSql('ALTER TABLE order_cake DROP FOREIGN KEY FK_B7E23B4A8D9F6D38');
        $this->addSql('ALTER TABLE savour_price DROP FOREIGN KEY FK_AFA356295F8DDB22');
        $this->addSql('ALTER TABLE cake DROP FOREIGN KEY FK_FA13015DDEDAC6BE');
        $this->addSql('ALTER TABLE pastry_chef_speciality DROP FOREIGN KEY FK_253FE3577153098');
        $this->addSql('ALTER TABLE supplement_price DROP FOREIGN KEY FK_6D179ED7A4D0F31E');
        $this->addSql('ALTER TABLE topping_price DROP FOREIGN KEY FK_AD16EC795F373D97');
        $this->addSql('ALTER TABLE cake DROP FOREIGN KEY FK_FA13015DA76ED395');
        $this->addSql('ALTER TABLE client_request DROP FOREIGN KEY FK_69AACBE2A76ED395');
        $this->addSql('ALTER TABLE client_request_message DROP FOREIGN KEY FK_E1BF1FDBA76ED395');
        $this->addSql('ALTER TABLE client_request_message DROP FOREIGN KEY FK_E1BF1FDB4B1D3F40');
        $this->addSql('ALTER TABLE client_request_proposal DROP FOREIGN KEY FK_6E09F1ACA76ED395');
        $this->addSql('ALTER TABLE client_request_proposal DROP FOREIGN KEY FK_6E09F1AC4B1D3F40');
        $this->addSql('ALTER TABLE colour_list DROP FOREIGN KEY FK_16B4F2D76B3CA4B');
        $this->addSql('ALTER TABLE orders DROP FOREIGN KEY FK_E52FFDEEA76ED395');
        $this->addSql('ALTER TABLE order_cake DROP FOREIGN KEY FK_B7E23B4AA76ED395');
        $this->addSql('ALTER TABLE pastry_chef_speciality DROP FOREIGN KEY FK_253FE35A76ED395');
        $this->addSql('ALTER TABLE review DROP FOREIGN KEY FK_794381C6A76ED395');
        $this->addSql('ALTER TABLE savour_list DROP FOREIGN KEY FK_9F65CE406B3CA4B');
        $this->addSql('ALTER TABLE supplement_list DROP FOREIGN KEY FK_FC5297FC6B3CA4B');
        $this->addSql('ALTER TABLE topping_list DROP FOREIGN KEY FK_474C94836B3CA4B');
        $this->addSql('DROP TABLE cake');
        $this->addSql('DROP TABLE price_cake');
        $this->addSql('DROP TABLE cake_size');
        $this->addSql('DROP TABLE candle_option');
        $this->addSql('DROP TABLE client_request');
        $this->addSql('DROP TABLE client_request_message');
        $this->addSql('DROP TABLE client_request_proposal');
        $this->addSql('DROP TABLE colour_active');
        $this->addSql('DROP TABLE colour_list');
        $this->addSql('DROP TABLE colour_price');
        $this->addSql('DROP TABLE dummy_stage_option');
        $this->addSql('DROP TABLE orders');
        $this->addSql('DROP TABLE order_cake');
        $this->addSql('DROP TABLE pastry_chef_speciality');
        $this->addSql('DROP TABLE review');
        $this->addSql('DROP TABLE savour_active');
        $this->addSql('DROP TABLE savour_list');
        $this->addSql('DROP TABLE savour_price');
        $this->addSql('DROP TABLE speciality_list');
        $this->addSql('DROP TABLE supplement_active');
        $this->addSql('DROP TABLE supplement_list');
        $this->addSql('DROP TABLE supplement_price');
        $this->addSql('DROP TABLE text_option');
        $this->addSql('DROP TABLE topping_active');
        $this->addSql('DROP TABLE topping_list');
        $this->addSql('DROP TABLE topping_price');
        $this->addSql('DROP TABLE user');
    }
}
