<?php
/**
 * Created by PhpStorm.
 * User: Djilan
 * Date: 29/09/2019
 * Time: 09:40
 */

namespace App\Controller\BackofficePastryChef;
use App\Entity\Cake;
use App\Entity\CakePrice;
use App\Entity\CandleOption;
use App\Entity\ClientRequestMessage;
use App\Entity\ClientRequestProposal;
use App\Entity\ColourActive;
use App\Entity\ColourList;
use App\Entity\ColourPrice;
use App\Entity\ClientRequest;
use App\Entity\DummyStageOption;
use App\Entity\SavourActive;
use App\Entity\SavourList;
use App\Entity\SavourPrice;
use App\Entity\SupplementActive;
use App\Entity\SupplementList;
use App\Entity\SupplementPrice;
use App\Entity\TextOption;
use App\Entity\ToppingActive;
use App\Entity\ToppingList;
use App\Entity\ToppingPrice;
use App\Form\ClientRequestMessageType;
use Intervention\Image\ImageManagerStatic as Image;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class BackofficeController extends AbstractController
{

    public function dashboard(Request $request){
        $entity_manager = $this->getDoctrine()->getManager();
        $cake_repo = $entity_manager->getRepository('App:Cake');
        $order_repo = $entity_manager->getRepository('App:OrderCake');

        $actif_cake = $cake_repo->findBy(array(
            'user' => $this->getUser()->getId(),
            'active' => 0
        ));

        $inactif_cake = $cake_repo->findBy(array(
            'user' => $this->getUser()->getId(),
            'active' => 1
        ));

        $orders = $order_repo->findByUser($this->getUser());

        return $this->render('backofficePastryChef/dashboard.html.twig', array(
            'actif_cakes' => count($actif_cake),
            'inactif_cakes' => count($inactif_cake),
            'orders' => count($orders),
        ));
    }

    public function editProfil(Request $request, UserPasswordEncoderInterface $encoder, $error)
    {
        $form_data = $request->request->all();
        $entity_manager = $this->getDoctrine()->getManager();
        $user_repo = $entity_manager->getRepository('App:User');
        $pastry_speciality_repo = $entity_manager->getRepository('App:PastryChefSpecialityList');
        $user = $user_repo->findOneById($this->getUser()->getId());
        $result = $this->uploadImage();
        $pastry_speciality = $pastry_speciality_repo->findByUser($this->getUser());

        if(!empty($form_data)){

            //MAJ DU CLIENT
            if(!$result['error']) {
                if(!is_null($result['result'])){
                    $user->setPicture($result['result']);
                }
            }
            else{
                return $this->render('backofficePastryChef/edit-profil.html.twig', array(
                    'user' => $user,
                    'speciality_list' => $pastry_speciality,
                    'error' => $result['result']
                ));
            }

            //Gestion du mot de passe
            if(!empty($form_data['old_password'])){
                if($encoder->isPasswordValid($user, $form_data['old_password'])) {
                    if(empty($form_data['new_password'])){
                        return $this->render('backofficePastryChef/edit-profil.html.twig', array(
                            'user' => $user,
                            'speciality_list' => $pastry_speciality,
                            'error' => 3
                        ));
                    }elseif(strlen($form_data['new_password']) <= 6){
                        return $this->render('backofficePastryChef/edit-profil.html.twig', array(
                            'user' => $user,
                            'speciality_list' => $pastry_speciality,
                            'error' => 2
                        ));
                    }
                    $encoded = $encoder->encodePassword($user, $form_data['new_password']);
                    $user->setPassword($encoded);
                }else{
                    return $this->render('backofficePastryChef/edit-profil.html.twig', array(
                        'user' => $user,
                        'speciality_list' => $pastry_speciality,
                        'error' => 1
                    ));
                }
            }

            $user->setRaisonSociale($form_data['company-name']);
            $user->setName($form_data['name']);
            $user->setFirstName($form_data['firstName']);
            $user->setPhone($form_data['phone']);
            $user->setAddress($form_data['address']);
            $user->setLocalisation($form_data['coordonnates']);
            $active = (isset($form_data['active'])) ? 1 : 0;
            $user->setActive($active);
//            $user->setZipCode($form_data['zipCode']);
            $user->setCity($form_data['city']);
            //MAJ DES JOURS
            $days = [];
            foreach ($form_data as $key => $item) {
                if(strpos($key, 'day') !== false){
                    $days[] = $item;
                }
            }
            $user->setDays($days);
            $entity_manager->persist($user);

            //MAJ DE LA SPECIALITE
            $ids = [];
            foreach ($form_data as $key => $item) {
                if(strpos($key, 'speciality') !== false){
                    //Mettre à jour le cake correspondant
                    $speciality_item = $pastry_speciality_repo->findOneBy(array('user' => $this->getUser(), 'id' => $item));
                    $speciality_item->setEtat(1);
                    $entity_manager->persist($speciality_item);
                    $ids[] = $item;
                }
            }

            $specialities = $pastry_speciality_repo->findBy(array('user' => $this->getUser()));

            foreach ($specialities as $speciality) {
                if(!in_array($speciality->getId(), $ids)){
                    $speciality->setEtat(0);
                    $entity_manager->persist($speciality);
                }
            }

            $entity_manager->flush();

            $error = 20;
        }

        return $this->render('backofficePastryChef/edit-profil.html.twig', array(
            'user' => $user,
            'speciality_list' => $pastry_speciality,
            'error' => $error
        ));
    }

    public function preAddCakeManagement(Request $request){
        //Afficher les valeurs
        return $this->render('backofficePastryChef/pre-add-cake-management.html.twig', array());
    }

    public function showCakeManagement()
    {

        $entity_manager = $this->getDoctrine()->getManager();
        $pastry_speciality_repo = $entity_manager->getRepository('App:PastryChefSpecialityList');
        $pastry_speciality = $pastry_speciality_repo->findByUser($this->getUser());
        $cake_repo = $entity_manager->getRepository('App:Cake');
        $cakes = $cake_repo->findBy(array(
            'user' => $this->getUser()->getId(),
            'isDel' => 0
        ));

        $flag = 0;
        foreach ($pastry_speciality as $speciality) {
            if($speciality->getEtat() == 1){
                $flag = 1;
                break;
            }
        }

        if($flag == 0){
            return $this->redirectToRoute('backoffice_pastry_chef_edit_profil', array('error' => 10));
        }

        return $this->render('backofficePastryChef/show-cake.html.twig', array(
            'cakes' => $cakes
        ));
    }

    public function addCakeManagement(Request $request){
        $entity_manager= $this->getDoctrine()->getManager();
        $pastry_chef_speciality_repo = $entity_manager->getRepository('App:PastryChefSpecialityList');

        $specialities = $pastry_chef_speciality_repo->findBy(array(
            'user' => $this->getUser()->getId(),
            'etat' => 1
        ));

        return $this->render('backofficePastryChef/add-cake-management.html.twig',array(
            'specialities' => $specialities
        ));
    }

    public function addCake(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        $pastry_chef_speciality_repo = $entityManager->getRepository('App:PastryChefSpecialityList');

        $params = $request->request->all();

        if(!is_null($this->getUser()) && !empty($params)){
            $img = [];
            $name = $params['name'];
            $description = $params['description'];
            $speciality_id = htmlentities($params['speciality']);
            $type_patisserie = $params['type_patisserie'];
            $type_prix = $params['type_prix'];
            $quantity_min = 0;
            $quantity_max = 0;
            $part_odd_even = (isset($params['part_odd_even']) ? 1 : 0);
            $quantity = 0;
            if($type_prix == "1"){
                $price = floatval($params['price_unit']);
                $quantity_min = intval($params['quantity_min']);
                $quantity_max = intval($params['quantity_max']);
            }
            else{
                $price = floatval($params['price_group']);
                $quantity = $params['quantity'];
            }
            $duration = $params['duration'];
            $dayWeek = $params['day_week'];
            $allergy = $params['allergy'];
            $categorie = $params['categorie'];

            $specialities = $pastry_chef_speciality_repo->findBy(array(
                'user' => $this->getUser()->getId(),
                'etat' => 1
            ));

            //Verification sur le prix
            if(is_float($price)){
                //$result = $this->uploadImage();
                $photo = $request->files->get('fichier');
                if ($photo) {
                    $newFilename = $this->uploadFile($photo, 'cake_photo_directory', 500, 'cake_photo_compressed_directory', 270
                    );

                    $speciality_repo = $entityManager->getRepository('App:SpecialityList');
                    $speciality = $speciality_repo->findOneById($speciality_id);
                    $img[] = array('url' => $newFilename, 'default' => 1);
                    $cake = new Cake();
                    $cake->setName($name);
                    $cake->setUser($this->getUser());
                    $cake->setDescription($description);
                    $cake->setIngredient("");
                    $cake->setTypePrix($type_prix);
                    $cake->setTypePatisserie($type_patisserie);
                    $cake->setDuration($duration);
                    $cake->setDayWeek($dayWeek);
                    $cake->setAllergy($allergy);
                    $cake->setCategorie($categorie);
                    $cake->setSpeciality($speciality);
                    $cake->setUrlImage($img);
                    $cake->setDummyStage(0);
                    $cake->setSavour(0);
                    $cake->setTopping(0);
                    $cake->setColour(0);
                    $cake->setSupplement(0);
                    $cake->setCandle(0);
                    $cake->setText(0);
                    $cake->setTopping(0);
                    $cake->setIsDel(0);
                    $cake->setActive(0);
                    $cake->setCreationDate((new \DateTime('now')));
                    $entityManager->persist($cake);

                    $cake_price = new CakePrice();
                    $cake_price->setSize(null);
                    $cake_price->setQuantityMin($quantity_min);
                    $cake_price->setQuantityMax($quantity_max);
                    $cake_price->setQuantity($quantity);
                    $cake_price->setOddEvenQuantity($part_odd_even);
                    $cake_price->setCake($cake);
                    $cake_price->setPrice($price);
                    $entityManager->persist($cake_price);
                    $entityManager->flush();

                    return $this->redirectToRoute('backoffice_pastry_old_add_cake_management',['id'=> $cake->getId()]);

                }else{
                    //TODO exception error
                    $error = 'Un problème est survenu lors de l\'enregistrement de l\'image';
                }

            }
            else{
                $error = 'Le prix est incorrect';
            }
        }

        return $this->render('backofficePastryChef/add-cake.html.twig', array(
            'error' => $error,
            'specialities' => $specialities
        ));

    }

    public function addCakePrice(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        $cake_repo = $entityManager->getRepository('App:Cake');
        $params = $request->request->all();
        $price = floatval($params['price']);
        $quantity = intval($params['quantity']);
        $cake_id = intval($params['cake_id']);

        $cake = $cake_repo->findOneById($cake_id);

        //Verification sur le prix
        $error = '';
        if(is_float($price)){

            //Ajout du prix du cake
            $cake_price = new CakePrice();
            $cake_price->setQuantity($quantity);
            $cake_price->setCake($cake);
            $cake_price->setPrice($price);
            $cake->setTypePrix(2);
            $entityManager->persist($cake_price);
            $entityManager->flush();

        }else{
            $error = 'Le prix est incorrect';
        }

        return new JsonResponse([
            'price' => $cake_price->getPrice(),
            'price_id' => $cake_price->getId(),
            'quantity' => $cake_price->getQuantity(),
            'error' => $error
        ]);

    }

    public function oldAddCakeManagement(int $id){
        //Afficher les valeurs
        return $this->render('backofficePastryChef/finish-add-cake-management.html.twig', array(
            'id' => $id
        ));
    }

    public function editCakeForm($id)
    {
        $entity_manager = $this->getDoctrine()->getManager();
        $cake_repo = $entity_manager->getRepository('App:Cake');
        $pastry_chef_speciality_repo = $entity_manager->getRepository('App:PastryChefSpecialityList');

        $cake = $cake_repo->findOneBy(array(
            'id' => $id,
            'user' => $this->getUser()->getId()
        ));

        $cake_price_repo = $entity_manager->getRepository('App:CakePrice');

        $specialities = $pastry_chef_speciality_repo->findBy(array(
            'user' => $this->getUser()->getId(),
            'etat' => 1
        ));

        if(!is_null($cake)){
            $prices = $cake_price_repo->findBy(array(
                'cake' => $cake->getId()
            ));

            //Afficher les valeurs

            return $this->render('backofficePastryChef/edit-cake-management.html.twig',array(
                'cake' => $cake,
                'prices' => $prices,
                'specialities' => $specialities
            ));
        }
    }

    public function showCakeAddForm()
    {
        $entity_manager= $this->getDoctrine()->getManager();
        $pastry_chef_speciality_repo = $entity_manager->getRepository('App:PastryChefSpecialityList');
        $cake_size_repo = $entity_manager->getRepository('App:CakeSize');

        $specialities = $pastry_chef_speciality_repo->findBy(array(
            'user' => $this->getUser()->getId(),
            'etat' => 1
        ));

        $sizes = $cake_size_repo->findAll();

        return $this->render('backofficePastryChef/add-cake.html.twig',array(
            'specialities' => $specialities,
            'sizes' => $sizes
        ));
    }

    public function editCake(Request $request, $id){

        $entity_manager= $this->getDoctrine()->getManager();
        $params = $request->request->all();
        $cake_repo = $entity_manager->getRepository('App:Cake');
        $cake_price_repo = $entity_manager->getRepository('App:CakePrice');
        $cake = $cake_repo->findOneBy(array(
            'id' => $id,
            'user' => $this->getUser()->getId()
        ));
        $cake_price = $cake_price_repo->findOneBy(array(
            'cake' => $cake,
            'size' => null
        ));
        if(empty($params)){
            return $this->redirectToRoute('backoffice_pastry_chef_edit_cake_form', array('id' => $id));
        }

        list ($cake, $error) = $this->addEditCake($request, $params, $cake, $cake_price, 0);

        if(!is_null($cake)){

            if($error == ''){
                $success = 'Modification réalisée avec succès';
                return $this->redirectToRoute('backoffice_pastry_chef_edit_cake_form', array('id' => $id, 'message' => $success));

            }else{
                return $this->redirectToRoute('backoffice_pastry_chef_edit_cake_form', array('id' => $id, 'message' => $error));

            }

        }

        return $this->redirectToRoute('backoffice_pastry_chef_edit_cake_form', array('id' => $id, 'success' => 'Modification réalisée avec succès'));
    }

    function addEditCake($request, $params, $cake = null, $cake_price = null, $add){
        $entityManager = $this->getDoctrine()->getManager();
        $error = '';
        if(!is_null($this->getUser()) && !empty($params)){
            $img = [];
            $name = $params['name'];
            $description = $params['description'];
            $speciality_id = htmlentities($params['speciality']);
            $type_patisserie = $params['type_patisserie'];
            $type_prix = $params['type_prix'];
            $quantity_min = 0;
            $quantity_max = 0;
            $part_odd_even = (isset($params['part_odd_even']) ? 1 : 0);
            $quantity = 0;
            $price = 0;

            if($type_prix == "1"){
                $price = floatval($params['price_unit']);
                $quantity_min = intval($params['quantity_min']);
                $quantity_max = intval($params['quantity_max']);
            }
            else {
                if ($add == 1){
                    $price = floatval($params['price_group']);
                    $quantity = $params['quantity'];
                }
            }
            $duration = $params['duration'];
            $dayWeek = $params['day_week'];
            $allergy = $params['allergy'];
            $categorie = $params['categorie'];

            if($add == 1){
                $photo = $request->files->get('fichier');
                if ($photo) {
                    $newFilename = $this->uploadFile($photo, 'cake_photo_directory', 500, 'cake_photo_compressed_directory', 270
                    );
                    $img[] = array('url' => $newFilename, 'default' => 1);
                }else{
                    //TODO exception error
                    $error = 'Un problème est survenu lors de l\'enregistrement de l\'image';
                }
            }

            $speciality_repo = $entityManager->getRepository('App:SpecialityList');
            $speciality = $speciality_repo->findOneById($speciality_id);
            if(is_null($cake)){
                $cake = new Cake();
            }

            $cake->setName($name);
            $cake->setUser($this->getUser());
            $cake->setDescription($description);
            $cake->setIngredient("");
            $cake->setTypePrix($type_prix);
            $cake->setTypePatisserie($type_patisserie);
            $cake->setDuration($duration);
            $cake->setDayWeek($dayWeek);
            $cake->setAllergy($allergy);
            $cake->setCategorie($categorie);
            $cake->setSpeciality($speciality);
            if($add == 1) {
                $cake->setUrlImage($img);
                $cake->setDummyStage(0);
                $cake->setSavour(0);
                $cake->setTopping(0);
                $cake->setColour(0);
                $cake->setSupplement(0);
                $cake->setCandle(0);
                $cake->setText(0);
                $cake->setTopping(0);
                $cake->setIsDel(0);
                $cake->setActive(0);
                $cake->setCreationDate((new \DateTime('now')));
            }
            $entityManager->persist($cake);

            if(is_null($cake_price)){
                $cake_price = new $cake_price();
            }
            if(($add == 1 || ($add == 0 && $type_prix == "1") && is_float($price))){
                $cake_price->setSize(null);
                $cake_price->setQuantityMin($quantity_min);
                $cake_price->setQuantityMax($quantity_max);
                $cake_price->setQuantity($quantity);
                $cake_price->setOddEvenQuantity($part_odd_even);
                $cake_price->setCake($cake);
                $cake_price->setPrice($price);
                $entityManager->persist($cake_price);
            }

            $entityManager->flush();
        }else{
            $error = 'Une erreur est survenue';
        }

        return [$cake,$error];
    }

    public function removeCake(Request $request, $id){

        $entity_manager= $this->getDoctrine()->getManager();
        $cake_repo = $entity_manager->getRepository('App:Cake');
        $cake = $cake_repo->findOneBy(array(
            'id' => $id,
            'user' => $this->getUser()->getId()
        ));
        $cake->setIsDel(1);
        $entity_manager->persist($cake);
        $entity_manager->flush();

        return $this->redirectToRoute('backoffice_pastry_chef_cake_management');

    }

    public function addImageCake(Request $request){

        $params = $request->request->all();

        $entity_manager= $this->getDoctrine()->getManager();
        $cake_repo = $entity_manager->getRepository('App:Cake');
        $cake = $cake_repo->findOneBy(array(
            'id' => $params['idCake'],
            'user' => $this->getUser()->getId(),
            'isDel' => 0
        ));
        $error = '';
        $new_image = [];
        $counter = 0;

        if(!is_null($cake)){
            $img = array_values($cake->getUrlImage());
            $photo = $request->files->get('fichier');
            if ($photo) {
                if(count($img) < 3) {
                    $newFilename = $this->uploadFile($photo, 'cake_photo_directory', 500, 'cake_photo_compressed_directory', 270
                    );
                    $img[] = array('url' => $newFilename, 'default' => 0);
                }else{
                    $error = '3 images maximum peuvent être ajoutées.';
                }

                if($error == ''){
                    if(count($img) == 1){
                        $img[0]['default'] = 1;
                    }

                    $flag = 0;
                    foreach ($img as $image) {
                        if($image['default'] == 1){
                            $flag = 1;
                        }
                    }

                    if($flag == 0){
                        $img[0]['default'] = 1;
                    }

                    $img_final = [];
                    foreach ($img as $key2 => $image) {
                        $img_final[] = $image;
                        $new_image[$key2]['url'] = 'img/pastry-chef/cake/photo/'.$this->getUser()->getId().'/'.$image['url'];
                        $new_image[$key2]['default'] = $image['default'];
                    }

                    $cake->setUrlImage($img_final);
                    $entity_manager->persist($cake);
                    $entity_manager->flush();
                }

            }

        }
        else{
            $error = 'Une erreur est survenue, veuillez actualiser la page';
        }

        return new JsonResponse([
            'error' => $error,
            'img' => $new_image
        ]);

    }

    public function removeImageCake(Request $request){

        $params = $request->request->all();

        $entity_manager= $this->getDoctrine()->getManager();
        $cake_repo = $entity_manager->getRepository('App:Cake');
        $cake = $cake_repo->findOneBy(array(
            'id' => $params['idCake'],
            'user' => $this->getUser()->getId(),
            'isDel' => 0
        ));

        $error = '';
        $img = array_values($cake->getUrlImage());



        if(count($img) == 1){
            $error = '1 image minimum est requise';
        }

        if($error == ''){
            if($img[$params['idImage']]['default'] == 1){
                $error = 'Changer d\'image par défaut avant de la supprimer';
            }else{
                unset($img[$params['idImage']]);
                $cake->setUrlImage($img);
                $entity_manager->persist($cake);
                $entity_manager->flush();
            }

        }

        return new JsonResponse([
            'error' => $error,
            'id' => $params['idImage']
        ]);

    }

    public function defaultImageCake(Request $request){

        $params = $request->request->all();

        $entity_manager= $this->getDoctrine()->getManager();
        $cake_repo = $entity_manager->getRepository('App:Cake');
        $cake = $cake_repo->findOneBy(array(
            'id' => $params['idCake'],
            'user' => $this->getUser()->getId(),
            'isDel' => 0
        ));

        $error = '';
        $img = array_values($cake->getUrlImage());

        $new_img = [];
        foreach ($img as $key => $item) {
            if ($params['idImage'] == $key) {
                $item['default'] = 1;
            } else {
                $item['default'] = 0;
            }
            $new_img[] = $item;
        }

        $img = $new_img;
        $cake->setUrlImage($img);
        $entity_manager->persist($cake);
        $entity_manager->flush();

        return new JsonResponse([
            'error' => $error,
            'id' => $params['idImage']
        ]);

    }

    public function showOrder()
    {
        $entity_manager= $this->getDoctrine()->getManager();
        $order_repo = $entity_manager->getRepository('App:OrderCake');
        $orders = $order_repo->findByUser($this->getUser());
        foreach ($orders as &$order) {
            $dummy_stage_tab = explode('||', $order->getDummyStage());
            $colour_tab = explode('||', $order->getColour());
            $savour_tab = explode('||', $order->getSavour());
            $topping_tab = explode('||', $order->getTopping());
            $text_tab = explode('||', $order->getText());
            $supplement_tab = explode('||', $order->getSupplement());
            $candle_tab = explode('||', $order->getCandle());
            if(count($dummy_stage_tab) > 1) $order->setDummyStage($dummy_stage_tab[0].' - '.$dummy_stage_tab[1]);
            if(count($colour_tab) > 1) $order->setColour($colour_tab[0].' - '.$colour_tab[1]);
            if(count($savour_tab) > 1) $order->setSavour($savour_tab[0].' - '.$savour_tab[1]);
            if(count($topping_tab) > 1) $order->setTopping($topping_tab[0].' - '.$topping_tab[1]);
            if(count($text_tab) > 1) $order->setText($text_tab[0].' - '.$text_tab[1]);
            if(count($supplement_tab) > 1) $order->setSupplement($supplement_tab[0].' - '.$supplement_tab[1]);
            if(count($candle_tab) > 1) $order->setCandle($candle_tab[0].' - '.$candle_tab[1]);
        }

        return $this->render('backofficePastryChef/show-order.html.twig',array(
            'orders' => $orders
        ));
    }

    public function showTraining()
    {
        return $this->render('backofficePastryChef/show-training.html.twig');
    }

    private function uploadImage(){
// Constantes
        define('TARGET', '\img');    // Repertoire cible
        define('MAX_SIZE', 2000000);    // Taille max en octets du fichier
        define('WIDTH_MAX', 1280);    // Largeur max de l'image en pixels
        define('HEIGHT_MAX', 1020);    // Hauteur max de l'image en pixels

// Tableaux de donnees
        $tabExt = array('jpg','png','jpeg');    // Extensions autorisees

// Variables
        $message = '';
        $success = '';

        /************************************************************
         * Script d'upload
         *************************************************************/

        // On verifie si le champ est rempli
        if(!empty($_FILES['fichier']['name']))
        {
            // Recuperation de l'extension du fichier
            $extension  = pathinfo($_FILES['fichier']['name'], PATHINFO_EXTENSION);

            // On verifie l'extension du fichier
            if(in_array(strtolower($extension),$tabExt))
            {
                // On recupere les dimensions du fichier
                $infosImg = getimagesize($_FILES['fichier']['tmp_name']);

                // On verifie le type de l'image
                if($infosImg[2] >= 1 && $infosImg[2] <= 14)
                {
                    if((filesize($_FILES['fichier']['tmp_name']) <= MAX_SIZE))
                    {
                        // Parcours du tableau d'erreurs
                        if(isset($_FILES['fichier']['error'])
                            && UPLOAD_ERR_OK === $_FILES['fichier']['error'])
                        {
                            // On renomme le fichier
                            $nomImage = md5(uniqid()) .'.'. $extension;

                            // Si c'est OK, on teste l'upload
                            if(!file_exists(getcwd().'/img/pastry-chef')) {
                                mkdir(getcwd().'/img/pastry-chef');
                            }

                            if(!file_exists(getcwd().'/img/pastry-chef/1')) {
                                mkdir(getcwd().'/img/pastry-chef/1');
                            }

                            if (move_uploaded_file($_FILES['fichier']['tmp_name'], getcwd().'/img/pastry-chef/1/' . $nomImage)) {
                                $success = "/img/pastry-chef/1/" . $nomImage;
                            } else {
                                // Sinon on affiche une erreur systeme
                                $message = 'Problème lors de l\'upload !';
                            }
                        }
                        else
                        {
                            $message = 'Une erreur interne a empêché l\'uplaod de l\'image';
                        }
                    }
                    else
                    {
                        // Sinon erreur sur les dimensions et taille de l'image
                        $message = 'Le poids de l\'image ne doit pas dépasser 2Mo !';
                    }
                }
                else
                {
                    // Sinon erreur sur le type de l'image
                    $message = 'Le fichier à uploader n\'est pas une image !';
                }
            }
            else
            {
                // Sinon on affiche une erreur pour l'extension
                $message = 'L\'extension du fichier est incorrecte !';
            }
        }else{
            return null;
        }

        if($message != ''){
            return array(
                'error' => true,
                'result' => $message
            );
        }else{
            return array(
                'error' => false,
                'result' => $success
            );
        }
    }

    private function uploadFile($photo, $photo_directory, $width, $photo_compress_directory = null, $width_compressed = 0){
        $originalFilename = pathinfo($photo->getClientOriginalName(), PATHINFO_FILENAME);
        // this is needed to safely include the file name as part of the URL
        $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
        $newFilename = $safeFilename . '-' . uniqid() . '.' . $photo->guessExtension();
        // Move the file to the directory where photo are stored
        try {
            $filesystem = new Filesystem();
            $photo->move(
                $this->getParameter($photo_directory). '/' .$this->getUser()->getId(),
                $newFilename
            );

            if($width > 0){
                $manager_picture = Image::make($this->getParameter($photo_directory) . '/' .$this->getUser()->getId(). '/' .$newFilename);
                // to finally create image instances
                $manager_picture->resize($width, null, function ($constraint) {
                    $constraint->aspectRatio();
                });

                $manager_picture->save($this->getParameter($photo_directory) . '/' .$this->getUser()->getId(). '/' . $newFilename);

                if(!is_null($photo_compress_directory)){
                    $filesystem->copy(
                        $this->getParameter($photo_directory).'/' .$this->getUser()->getId(). '/'.$newFilename,
                        $this->getParameter($photo_compress_directory).'/' .$this->getUser()->getId(). '/'.$newFilename
                    );
                    //http://image.intervention.io/api/resize
                    $manager_picture2 = Image::make($this->getParameter($photo_compress_directory) . '/' .$this->getUser()->getId(). '/' . $newFilename);
                    $manager_picture2->resize($width_compressed, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                    $manager_picture2->save($this->getParameter($photo_compress_directory) . '/' .$this->getUser()->getId(). '/' . $newFilename);
                }
            }


            return $newFilename;


        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
        }
    }

    public function removeCakePrice(Request $request){

        $entity_manager= $this->getDoctrine()->getManager();
        $cake_price_repo = $entity_manager->getRepository('App:CakePrice');
        $cake_id = $request->request->get('cake-id');
        $price_cake_id = $request->request->get('price');

        $cake_repo = $entity_manager->getRepository('App:Cake');
        $cake = $cake_repo->findOneBy(array(
            'id' => $cake_id,
            'user' => $this->getUser()->getId()
        ));

        if(!is_null($cake)){
            $price = $cake_price_repo->findOneBy(array(
                'id' => $price_cake_id,
                'cake' => $cake
            ));

            $entity_manager->remove($price);
            $entity_manager->flush();

            return new JsonResponse(['error' => '']);
        }else{
            return new JsonResponse(['error' => 'Suppression non ok']);
        }

    }

    public function validateOrder(Request $request){

        $entity_manager= $this->getDoctrine()->getManager();
        $order_repo = $entity_manager->getRepository('App:Order');
        $order_cake_repo = $entity_manager->getRepository('App:OrderCake');
        $orders = $order_cake_repo->findByUser($this->getUser());
        $params = $request->request->all();
        $order = $order_repo->findOneById($params['order_id']);

        if($order->getCode() == $params['code']){
            $order->setValidate(1);
            $entity_manager->persist($order);
            $entity_manager->flush();
            $error = false;
        }else{
            $error = true;
        }

        foreach ($orders as &$order) {
            $dummy_stage_tab = explode('||', $order->getDummyStage());
            $colour_tab = explode('||', $order->getColour());
            $savour_tab = explode('||', $order->getSavour());
            $topping_tab = explode('||', $order->getTopping());
            $text_tab = explode('||', $order->getText());
            $supplement_tab = explode('||', $order->getSupplement());
            $candle_tab = explode('||', $order->getCandle());
            if(count($dummy_stage_tab) > 1) $order->setDummyStage($dummy_stage_tab[0].' - '.$dummy_stage_tab[1]);
            if(count($colour_tab) > 1) $order->setColour($colour_tab[0].' - '.$colour_tab[1]);
            if(count($savour_tab) > 1) $order->setSavour($savour_tab[0].' - '.$savour_tab[1]);
            if(count($topping_tab) > 1) $order->setTopping($topping_tab[0].' - '.$topping_tab[1]);
            if(count($text_tab) > 1) $order->setText($text_tab[0].' - '.$text_tab[1]);
            if(count($supplement_tab) > 1) $order->setSupplement($supplement_tab[0].' - '.$supplement_tab[1]);
            if(count($candle_tab) > 1) $order->setCandle($candle_tab[0].' - '.$candle_tab[1]);
        }

        return $this->render('backofficePastryChef/show-order.html.twig',array(
            'orders' => $orders,
            'error' => $error
        ));
    }

    public function feature(){
        return $this->render('backofficePastryChef/feature.html.twig');
    }

    public function showClientRequest(){
        $entity_manager= $this->getDoctrine()->getManager();
        $client_request_repo = $entity_manager->getRepository('App:ClientRequest');
        $clients_request = $client_request_repo->findAll();

        //Verification si la demande est dans le secteur du pâtissier
        $coordinates_patry_chef = $this->getUser()->getLocalisation();
        $coordinates_patry_chef_tab = explode('-', $coordinates_patry_chef);
        $lng = $coordinates_patry_chef_tab[0];
        $lat = $coordinates_patry_chef_tab[1];

        foreach ($clients_request as $key => $client_request) {
            $coordinates_client_request = $client_request->getCoordinnates();

            if(!is_null($coordinates_client_request)){
                $coordinates_client_request_tab = explode('-', $coordinates_client_request);

                $lng_client = $coordinates_client_request_tab[0];
                $lat_client = $coordinates_client_request_tab[1];

                $distance = $this->distanceGeoPoints($lng_client, $lat_client, $lng, $lat);

                if($distance > 30){
                    unset($clients_request[$key]);
                }
            }
        }

        return $this->render('backofficePastryChef/show-client-request.html.twig',array(
            'clients_request' => $clients_request
        ));
    }

    public function showClientRequestInbox(ClientRequest $clientRequest, Request $request){

        $entity_manager= $this->getDoctrine()->getManager();
        $client_request_repo = $entity_manager->getRepository('App:ClientRequest');
        $client_request_proposal_repo = $entity_manager->getRepository('App:ClientRequestProposal');
        $client_request_proposal = $client_request_proposal_repo->findOneBy(['clientRequest' => $clientRequest], ['id' => 'DESC'], 1);

        $client_request_message = new ClientRequestMessage();
        $form = $this->createForm(ClientRequestMessageType::Class, $client_request_message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && !empty($_FILES)) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $client_request_message = $form->getData();

            $client_request_value = $client_request_repo->findOneById($clientRequest);
            $session = $this->get('session');
            if(!is_null($client_request_value) && $session->get('client_request') == $clientRequest->getId()){
                /** @var UploadedFile $brochureFile */
                $photo = $form->get('photo')->getData();

                // this condition is needed because the 'brochure' field is not required
                // so the PDF file must be processed only when a file is uploaded
                if ($photo) {
                    $originalFilename = pathinfo($photo->getClientOriginalName(), PATHINFO_FILENAME);
                    // this is needed to safely include the file name as part of the URL
                    $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                    $newFilename = $safeFilename . '-' . uniqid() . '.' . $photo->guessExtension();

                    // Move the file to the directory where photo are stored
                    try {

                        $photo->move(
                            $this->getParameter('photo_message_directory'),
                            $newFilename
                        );

                        //http://image.intervention.io/api/resize
                        $manager = Image::make($this->getParameter('photo_message_directory') . '/' . $newFilename);

                        // to finally create image instances
                        $manager->resize(150, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });

                        $manager->save($this->getParameter('photo_message_directory') . '/' . $newFilename);
                    } catch (FileException $e) {
                        // ... handle exception if something happens during file upload
                    }

                    // updates the 'brochureFilename' property to store the PDF file name
                    // instead of its contents

                    $date = new \DateTime('now');
                    $client_request_message = new ClientRequestMessage();
                    $client_request_message->setClientRequest($client_request_value);
                    $client_request_message->setCreationDate($date);
                    $client_request_message->setUser($client_request_value->getUser());
                    $client_request_message->setUserPastry($this->getUser());
                    $client_request_message->setPhoto($newFilename);
                    $entity_manager->persist($client_request_message);
                    $entity_manager->flush();
                    return $this->redirectToRoute("backoffice_pastry_chef_client_request_inbox",['clientRequest' => $clientRequest->getId()]);
                }

            }

        }
        else{
            $session = $this->get('session');
            $session->set('client_request', $clientRequest->getId());
        }

        return $this->render('backofficePastryChef/show-client-request-inbox.html.twig',array(
            'client_request' => $clientRequest,
            'client_request_proposal' => $client_request_proposal,
            'form' => $form->createView()
        ));
    }

    public function addMessageClientRequest(Request $request){

        $entity_manager= $this->getDoctrine()->getManager();
        $message = strip_tags($request->request->get('message'));
        $client_request = strip_tags($request->request->get('client_request'));
        $client_request_repo = $entity_manager->getRepository('App:clientRequest');
        $client_request_value = $client_request_repo->findOneById($client_request);
        $session = $this->get('session');
        if(!is_null($client_request_value) && $session->get('client_request') == $client_request){
            $date = new \DateTime('now');
            $client_request_message = new ClientRequestMessage();
            $client_request_message->setClientRequest($client_request_value);
            $client_request_message->setCreationDate($date);
            $client_request_message->setMessage($message);
            $client_request_message->setUserPastry($this->getUser());
            $client_request_message->setUser($client_request_value->getUser());
            $client_request_message->setWho('pastry-chef');

            $entity_manager->persist($client_request_message);
            $entity_manager->flush();
            $user_string = $this->getUser()->getRaisonSociale().' - '.$this->getUser()->getName().' '.$this->getUser()->getFirstName().' '.$date->format('d/m/Y'). ' '.$date->format('H:i:s');

            return new JsonResponse(['result' => true, 'message' => $message, 'user_string' => $user_string]);
        }else{
            return new JsonResponse(['result' => false]);
        }

    }

    public function getMessagesClientRequest(Request $request){

        $entity_manager= $this->getDoctrine()->getManager();
        $client_request = strip_tags($request->request->get('client_request'));
        $client_request_repo = $entity_manager->getRepository('App:clientRequest');
        $client_request_message_repo = $entity_manager->getRepository('App:clientRequestMessage');
        $client_request_value = $client_request_repo->findOneById($client_request);

        $session = $this->get('session');
        if(!is_null($client_request_value) && $session->get('client_request') == $client_request){
            $date = new \DateTime('now');
            $messages = $client_request_message_repo->findBy(['clientRequest' => $client_request, 'userPastry' => $this->getUser()]);
            $message_final = '';
            foreach ($messages as $message) {
                if(!is_null($message->getMessage())){
                    $message_string = '<p>'.$message->getMessage().'</p>';
                }else{
                    if(strpos($_SERVER['HTTP_HOST'],'localhost') !== false) $url = $this->getParameter('url_dev');
                    else $url = $this->getParameter('url_prod');
                    $message_string = '<img width="150px" src="'.$url.'img/client_request/'.$message->getPhoto().'"/>';
                }
                $user_string = '<span>'.$this->getUser()->getRaisonSociale().' - '.$this->getUser()->getName().' '.$this->getUser()->getFirstName().' '.$message->getCreationDate()->format('d/m/Y'). ' '.$message->getCreationDate()->format('H:i:s').'</span>';
                $message_final .= $message_string.$user_string;
            }
            return new JsonResponse(['result' => true, 'messages' => $message_final, 'count' => count($messages)]);
        }else{
            return new JsonResponse(['result' => false]);
        }

    }

    public function addPastryChefProposalClientRequest(Request $request){

        $entity_manager= $this->getDoctrine()->getManager();
        $client_request = strip_tags($request->request->get('client_request'));
        $price = strip_tags($request->request->get('proposeprice'));
        $client_request_repo = $entity_manager->getRepository('App:clientRequest');
        $client_request_value = $client_request_repo->findOneById($client_request);

        $session = $this->get('session');
        if(!is_null($client_request_value) && $session->get('client_request') == $client_request){

            $proposal = new ClientRequestProposal();
            $proposal->setUserPastry($this->getUser());
            $proposal->setUser($client_request_value->getUser());
            $proposal->setClientRequest($client_request_value);
            $proposal->setPrice($price);
            $proposal->setCreationDate((new \DateTime('now')));
            $entity_manager->persist($proposal);
            $entity_manager->flush();

            return $this->redirectToRoute('backoffice_pastry_chef_client_request_inbox', ['clientRequest' => $client_request_value->getId()]);

        }else{
            return $this->redirectToRoute('backoffice_pastry_chef_client_request');
        }
    }

//OPTION
    public function showCakeOption($id)
    {
        $entity_manager = $this->getDoctrine()->getManager();
        $cake_repo = $entity_manager->getRepository('App:Cake');
        $dummy_stage_option_repo = $entity_manager->getRepository('App:DummyStageOption');
        $candle_option_repo = $entity_manager->getRepository('App:CandleOption');
        $text_option_repo = $entity_manager->getRepository('App:TextOption');
        $savour_list_repo = $entity_manager->getRepository('App:SavourList');
        $savour_active_repo = $entity_manager->getRepository('App:SavourActive');
        $savour_price_repo = $entity_manager->getRepository('App:SavourPrice');
        $supplement_list_repo = $entity_manager->getRepository('App:SupplementList');
        $supplement_active_repo = $entity_manager->getRepository('App:SupplementActive');
        $supplement_price_repo = $entity_manager->getRepository('App:SupplementPrice');
        $topping_list_repo = $entity_manager->getRepository('App:ToppingList');
        $topping_active_repo = $entity_manager->getRepository('App:ToppingActive');
        $topping_price_repo = $entity_manager->getRepository('App:ToppingPrice');
        $colour_list_repo = $entity_manager->getRepository('App:ColourList');
        $colour_active_repo = $entity_manager->getRepository('App:ColourActive');
        $colour_price_repo = $entity_manager->getRepository('App:ColourPrice');
        $user = $this->getUser();

        //Récupération du bon gâteau
        $cake = $cake_repo->findOneBy(array(
            'id' => $id,
            'user' => $this->getUser()->getId()
        ));

        if(!is_null($cake)){
            /////////////Récupération des options//////////////
            /// Etage factice
            $dummy_stage_option = $dummy_stage_option_repo->findOneBy(array('cake' => $cake));

            ////// Saveurs //////
            $savour_list = $savour_list_repo->getSavourList($user->getId());
            $savour_active = $savour_active_repo->findOneBy(array('cake' => $cake));
            $savour_final = [];

            foreach ($savour_list as $key => $savour) {
                if(!is_null($savour_active)) {
                    $savour_list_tab = explode(',', $savour_active->getSavourList());
                    if(in_array($savour->getId(), $savour_list_tab)){
                        $savour_final[$key]['active'] = true;
                    }else{
                        $savour_final[$key]['active'] = false;
                    }
                }else{
                    $savour_final[$key]['active'] = false;
                }

                $savour_final[$key]['savour'] = $savour;
                $savour_final[$key]['price'] = $savour_price_repo->findOneBy(array('savour' => $savour->getId(), 'cake' => $cake));

            }

            /// Suppléments
            $supplement_list = $supplement_list_repo->getSupplementList($user->getId());
            $supplement_active = $supplement_active_repo->findOneBy(array('cake' => $cake));

            $supplement_final = [];

            foreach ($supplement_list as $key => $supplement) {
                if(!is_null($supplement_active)) {
                    $supplement_list_tab = explode(',', $supplement_active->getSupplementList());
                    if(in_array($supplement->getId(), $supplement_list_tab)){
                        $supplement_final[$key]['active'] = true;
                    }else{
                        $supplement_final[$key]['active'] = false;
                    }
                }else{
                    $supplement_final[$key]['active'] = false;
                }

                $supplement_final[$key]['supplement'] = $supplement;
                $supplement_final[$key]['price'] = $supplement_price_repo->findOneBy(array('supplement' => $supplement->getId(), 'cake' => $cake));

            }

            /// Nappage
            $topping_list = $topping_list_repo->getToppingList($user->getId());
            $topping_active = $topping_active_repo->findOneBy(array('cake' => $cake));

            $topping_final = [];

            foreach ($topping_list as $key => $topping) {
                if(!is_null($topping_active)) {
                    $topping_list_tab = explode(',', $topping_active->getToppingList());
                    if(in_array($topping->getId(), $topping_list_tab)){
                        $topping_final[$key]['active'] = true;
                    }else{
                        $topping_final[$key]['active'] = false;
                    }
                }else{
                    $topping_final[$key]['active'] = false;
                }

                $topping_final[$key]['topping'] = $topping;
                $topping_final[$key]['price'] = $topping_price_repo->findOneBy(array('topping' => $topping->getId(), 'cake' => $cake));

            }

            /// Couleur
            $colour_list = $colour_list_repo->getColourList($user->getId());
            $colour_active = $colour_active_repo->findOneBy(array('cake' => $cake));

            $colour_final = [];

            foreach ($colour_list as $key => $colour) {
                if(!is_null($colour_active)) {
                    $colour_list_tab = explode(',', $colour_active->getColourList());
                    if(in_array($colour->getId(), $colour_list_tab)){
                        $colour_final[$key]['active'] = true;
                    }else{
                        $colour_final[$key]['active'] = false;
                    }
                }else{
                    $colour_final[$key]['active'] = false;
                }

                $colour_final[$key]['colour'] = $colour;
                $colour_final[$key]['price'] = $colour_price_repo->findOneBy(array('colour' => $colour->getId(), 'cake' => $cake));

            }

            /// Bougie
            $candle_option = $candle_option_repo->findOneBy(array('cake' => $cake));

            /// Texte
            $text_option = $text_option_repo->findOneBy(array('cake' => $cake));

            //Afficher les valeurs
            return $this->render('backofficePastryChef/options-cake.html.twig', array(
                'cake' => $cake,
                'dummy_stage_option' => $dummy_stage_option,
                'candle_option' => $candle_option,
                'text_option' => $text_option,
                'savour_final' => $savour_final,
                'supplement_final' => $supplement_final,
                'topping_final' => $topping_final,
                'colour_final' => $colour_final
            ));
        }
    }

    public function activeOption(Request $request){
        $entity_manager = $this->getDoctrine()->getManager();
        $cake_repo = $entity_manager->getRepository('App:Cake');
        $params = $request->request->all();
        //Récupération du bon gâteau
        $cake = $cake_repo->findOneBy(array(
            'id' => $params['cake'],
            'user' => $this->getUser()->getId()
        ));

        if(isset($params['dummy-stage'])){
            $cake->setDummyStage(($params['dummy-stage'] == 'true') ? 1 : 0);
        }

        if(isset($params['topping'])){
            $cake->setTopping(($params['topping'] == 'true') ? 1 : 0);
        }

        if(isset($params['candle'])){
            $cake->setCandle(($params['candle'] == 'true') ? 1 : 0);
        }

        if(isset($params['savour'])){
            $cake->setSavour(($params['savour'] == 'true') ? 1 : 0);
        }

        if(isset($params['colour'])){
            $cake->setColour(($params['colour'] == 'true') ? 1 : 0);
        }

        if(isset($params['text'])){
            $cake->setText(($params['text'] == 'true') ? 1 : 0);
        }

        if(isset($params['supplement'])){
            $cake->setSupplement(($params['supplement'] == 'true') ? 1 : 0);
        }

        $entity_manager->persist($cake);
        $entity_manager->flush();

        return new JsonResponse(array('result' => 'ok'));
    }

    public function activeSousOption(Request $request){
        $entity_manager = $this->getDoctrine()->getManager();
        $cake_repo = $entity_manager->getRepository('App:Cake');
        $topping_active_repo = $entity_manager->getRepository('App:ToppingActive');
        $savour_active_repo = $entity_manager->getRepository('App:SavourActive');
        $colour_active_repo = $entity_manager->getRepository('App:ColourActive');
        $supplement_active_repo = $entity_manager->getRepository('App:SupplementActive');
        $params = $request->request->all();

        //Récupération du bon gâteau
        $cake = $cake_repo->findOneBy(array(
            'id' => $params['cake'],
            'user' => $this->getUser()->getId()
        ));

        if(isset($params['topping-value'])){
            $topping_active = $topping_active_repo->findOneByCake($params['cake']);
            if(!is_null($topping_active)){
                $topping_list_tab = explode(',', $topping_active->getToppingList());
                $topping_final = '';

                //Si il est présent dans la table active donc on doit le retirer
                if(in_array($params['topping-value'], $topping_list_tab)){
                    foreach ($topping_list_tab as $key => $item) {
                        if($params['topping-value'] == $item){
                            unset($topping_list_tab[$key]);
                        }else{
                            $topping_final .= $item.',';
                        }
                    }
                    $topping_final = substr($topping_final, 0, -1);
                }
                //Si il est pas présent dans la table active donc on doit l'ajouter
                else{
                    if($topping_active->getToppingList() != null or $topping_active->getToppingList() != ''){
                        $topping_final = $topping_active->getToppingList().','.$params['topping-value'];
                    }else{
                        $topping_final = $params['topping-value'];
                    }
                }

                $topping_active->setToppingList($topping_final);
                $entity_manager->persist($topping_active);

            }
            //Active non existant
            else{
                $topping_active = new ToppingActive();
                $topping_active->setCake($cake);
                $topping_active->setToppingList($params['topping-value']);
                $entity_manager->persist($topping_active);
            }
        }

        if(isset($params['savour-value'])){
            $savour_active = $savour_active_repo->findOneByCake($params['cake']);
            if(!is_null($savour_active)){
                $savour_list_tab = explode(',', $savour_active->getSavourList());
                $savour_final = '';

                //Si il est présent dans la table active donc on doit le retirer
                if(in_array($params['savour-value'], $savour_list_tab)){
                    foreach ($savour_list_tab as $key => $item) {
                        if($params['savour-value'] == $item){
                            unset($savour_list_tab[$key]);
                        }else{
                            $savour_final .= $item.',';
                        }
                    }
                    $savour_final = substr($savour_final, 0, -1);
                }
                //Si il est pas présent dans la table active donc on doit l'ajouter
                else{
                    if($savour_active->getSavourList() != null or $savour_active->getSavourList() != ''){
                        $savour_final = $savour_active->getSavourList().','.$params['savour-value'];
                    }else{
                        $savour_final = $params['savour-value'];
                    }
                }

                $savour_active->setSavourList($savour_final);
                $entity_manager->persist($savour_active);

            }
            //Active non existant
            else{
                $savour_active = new SavourActive();
                $savour_active->setCake($cake);
                $savour_active->setSavourList($params['savour-value']);
                $entity_manager->persist($savour_active);
            }
        }

        if(isset($params['colour-value'])){
            $colour_active = $colour_active_repo->findOneByCake($params['cake']);
            if(!is_null($colour_active)){
                $colour_list_tab = explode(',', $colour_active->getColourList());
                $colour_final = '';

                //Si il est présent dans la table active donc on doit le retirer
                if(in_array($params['colour-value'], $colour_list_tab)){
                    foreach ($colour_list_tab as $key => $item) {
                        if($params['colour-value'] == $item){
                            unset($colour_list_tab[$key]);
                        }else{
                            $colour_final .= $item.',';
                        }
                    }
                    $colour_final = substr($colour_final, 0, -1);
                }
                //Si il est pas présent dans la table active donc on doit l'ajouter
                else{
                    if($colour_active->getColourList() != null or $colour_active->getColourList() != ''){
                        $colour_final = $colour_active->getColourList().','.$params['colour-value'];
                    }else{
                        $colour_final = $params['colour-value'];
                    }
                }

                $colour_active->setColourList($colour_final);
                $entity_manager->persist($colour_active);

            }
            //Active non existant
            else{
                $colour_active = new ColourActive();
                $colour_active->setCake($cake);
                $colour_active->setColourList($params['colour-value']);
                $entity_manager->persist($colour_active);
            }
        }

        if(isset($params['supplement-value'])){
            $supplement_active = $supplement_active_repo->findOneByCake($params['cake']);
            if(!is_null($supplement_active)){
                $supplement_list_tab = explode(',', $supplement_active->getSupplementList());
                $supplement_final = '';

                //Si il est présent dans la table active donc on doit le retirer
                if(in_array($params['supplement-value'], $supplement_list_tab)){
                    foreach ($supplement_list_tab as $key => $item) {
                        if($params['supplement-value'] == $item){
                            unset($supplement_list_tab[$key]);
                        }else{
                            $supplement_final .= $item.',';
                        }
                    }
                    $supplement_final = substr($supplement_final, 0, -1);
                }
                //Si il est pas présent dans la table active donc on doit l'ajouter
                else{
                    if($supplement_active->getSupplementList() != null or $supplement_active->getSupplementList() != ''){
                        $supplement_final = $supplement_active->getSupplementList().','.$params['supplement-value'];
                    }else{
                        $supplement_final = $params['supplement-value'];
                    }
                }

                $supplement_active->setSupplementList($supplement_final);
                $entity_manager->persist($supplement_active);

            }
            //Active non existant
            else{
                $supplement_active = new SupplementActive();
                $supplement_active->setCake($cake);
                $supplement_active->setSupplementList($params['supplement-value']);
                $entity_manager->persist($supplement_active);
            }
        }

        $entity_manager->flush();

        return new JsonResponse(array('result' => 'ok'));
    }

    public function editPriceOption(Request $request){
        $entity_manager = $this->getDoctrine()->getManager();
        $cake_repo = $entity_manager->getRepository('App:Cake');
        $topping_price_repo = $entity_manager->getRepository('App:ToppingPrice');
        $topping_list_repo = $entity_manager->getRepository('App:ToppingList');
        $savour_price_repo = $entity_manager->getRepository('App:SavourPrice');
        $savour_list_repo = $entity_manager->getRepository('App:SavourList');
        $colour_price_repo = $entity_manager->getRepository('App:ColourPrice');
        $colour_list_repo = $entity_manager->getRepository('App:ColourList');
        $supplement_price_repo = $entity_manager->getRepository('App:SupplementPrice');
        $supplement_list_repo = $entity_manager->getRepository('App:SupplementList');
        $dummy_stage_option_repo = $entity_manager->getRepository('App:DummyStageOption');
        $candle_option_repo = $entity_manager->getRepository('App:CandleOption');
        $text_option_repo = $entity_manager->getRepository('App:TextOption');
        $params = $request->request->all();

        $cake = $cake_repo->findOneBy(array(
            'id' => $params['cake-id'],
            'user' => $this->getUser()->getId()
        ));

        //Récupération du bon gâteau
        if(!is_null($cake)) {

            // PRIX NAPPAGE
            if(isset($params['topping-id'])){
                $topping_price = $topping_price_repo->findOneBy(array('topping' => $params['topping-id'], 'cake' => $cake));
                $topping_list = $topping_list_repo->findOneById($params['topping-id']);
                //Si y a pas de prix
                if(is_null($topping_price)){
                    //On ajoute un nouveau prix
                    $topping_price = new ToppingPrice();
                    $topping_price->setPrice($params['topping-price']);
                    $topping_price->setTopping($topping_list);
                    $topping_price->setCake($cake);
                }
                else{
                    //On modifie le prix
                    $topping_price->setPrice($params['topping-price']);
                }
                //Si le user n'est pas null
                //On modifie le libelle
                if(!is_null($topping_list->getUser())){
                    $topping_list->setLibelle($params['topping-libelle']);
                    $entity_manager->persist($topping_list);
                }
                $entity_manager->persist($topping_price);
            }

            // PRIX COULEUR
            if(isset($params['colour-id'])){
                $colour_price = $colour_price_repo->findOneBy(array('colour' => $params['colour-id'], 'cake' => $cake));
                $colour_list = $colour_list_repo->findOneById($params['colour-id']);
                //Si y a pas de prix
                if(is_null($colour_price)){
                    //On ajoute un nouveau prix
                    $colour_price = new ColourPrice();
                    $colour_price->setPrice($params['colour-price']);
                    $colour_price->setColour($colour_list);
                    $colour_price->setCake($cake);
                }
                else{
                    //On modifie le prix
                    $colour_price->setPrice($params['colour-price']);
                }
                //Si le user n'est pas null
                //On modifie le libelle
                if(!is_null($colour_list->getUser())){
                    $colour_list->setLibelle($params['colour-libelle']);
                    $colour_list->setHexa($params['colour-hexa']);
                    $entity_manager->persist($colour_list);
                }
                $entity_manager->persist($colour_price);
            }

            // PRIX SAVEUR
            if(isset($params['savour-id'])) {
                $savour_price = $savour_price_repo->findOneBy(array('savour' => $params['savour-id'], 'cake' => $cake));
                $savour_list = $savour_list_repo->findOneById($params['savour-id']);
                //Si y a pas de prix
                if (is_null($savour_price)) {
                    //On ajoute un nouveau prix
                    $savour_price = new SavourPrice();
                    $savour_price->setPrice($params['savour-price']);
                    $savour_price->setSavour($savour_list);
                    $savour_price->setCake($cake);
                } else {
                    //On modifie le prix
                    $savour_price->setPrice($params['savour-price']);
                }
                //Si le user n'est pas null
                //On modifie le libelle
                if (!is_null($savour_list->getUser())) {
                    $savour_list->setLibelle($params['savour-libelle']);
                    $entity_manager->persist($savour_list);
                }
                $entity_manager->persist($savour_price);
            }

            // PRIX SUPPLEMEMENT
            if(isset($params['supplement-id'])){
                $supplement_price = $supplement_price_repo->findOneBy(array('supplement' => $params['supplement-id'], 'cake' => $cake));
                $supplement_list = $supplement_list_repo->findOneById($params['supplement-id']);
                //Si y a pas de prix
                if(is_null($supplement_price)){
                    //On ajoute un nouveau prix
                    $supplement_price = new SupplementPrice();
                    $supplement_price->setPrice($params['supplement-price']);
                    $supplement_price->setSupplement($supplement_list);
                    $supplement_price->setCake($cake);
                }else{
                    //On modifie le prix
                    $supplement_price->setPrice($params['supplement-price']);
                }
                //Si le user n'est pas null
                //On modifie le libelle
                if(!is_null($supplement_list->getUser())){
                    $supplement_list->setLibelle($params['supplement-libelle']);
                    $entity_manager->persist($supplement_list);
                }
                $entity_manager->persist($supplement_price);
            }

            // PRIX ETAGE FACTICE
            if(isset($params['dummy-stage-id'])){
                $dummy_stage_option = $dummy_stage_option_repo->findOneByCake($cake);
                if(!is_null($dummy_stage_option)){
                    $dummy_stage_option->setPrice($params['dummy-stage-price']);
                    $dummy_stage_option->setMax($params['dummy-stage-max']);
                }else{
                    $dummy_stage_option = new DummyStageOption();
                    $dummy_stage_option->setCake($cake);
                    $dummy_stage_option->setPrice($params['dummy-stage-price']);
                    $dummy_stage_option->setMax($params['dummy-stage-max']);
                }
                $entity_manager->persist($dummy_stage_option);
            }

            // PRIX BOUGIE
            if(isset($params['candle-option-id'])){
                $candle_option = $candle_option_repo->findOneByCake($cake);
                if(!is_null($candle_option)){
                    $candle_option->setPrice($params['candle-price']);
                    $candle_option->setMax($params['candle-max']);
                }else{
                    $candle_option = new CandleOption();
                    $candle_option->setCake($cake);
                    $candle_option->setPrice($params['candle-price']);
                    $candle_option->setMax($params['candle-max']);
                }
                $entity_manager->persist($candle_option);
            }

            // PRIX INSCRIPTION
            if(isset($params['text-option-id'])){
                $text_option = $text_option_repo->findOneByCake($cake);
                if(!is_null($text_option)){
                    $text_option->setPrice($params['text-price']);
                }else{
                    $text_option = new TextOption();
                    $text_option->setCake($cake);
                    $text_option->setPrice($params['text-price']);
                }
                $entity_manager->persist($text_option);
            }

            $entity_manager->flush();
        }

        return $this->redirectToRoute('backoffice_pastry_show_option', array('id' => $cake->getId()));
    }

    public function addPriceOption(Request $request){
        $entity_manager = $this->getDoctrine()->getManager();
        $cake_repo = $entity_manager->getRepository('App:Cake');
        $params = $request->request->all();

        $cake = $cake_repo->findOneBy(array(
            'id' => $params['cake-id'],
            'user' => $this->getUser()->getId()
        ));

        //Récupération du bon gâteau
        if(!is_null($cake)) {

            // PRIX NAPPAGE
            if(isset($params['topping-submit'])){
                //Topping liste
                $topping_list = new ToppingList();
                $topping_list->setLibelle($params['topping-libelle']);
                $topping_list->setUser($this->getUser());

                //On ajoute un nouveau prix
                $topping_price = new ToppingPrice();
                $topping_price->setPrice($params['topping-price']);
                $topping_price->setTopping($topping_list);
                $topping_price->setCake($cake);

                $entity_manager->persist($topping_price);
                $entity_manager->persist($topping_list);

            }

            // PRIX SAVEUR
            if(isset($params['savour-submit'])){
                //saveur liste
                $savour_list = new SavourList();
                $savour_list->setLibelle($params['savour-libelle']);
                $savour_list->setUser($this->getUser());

                //On ajoute un nouveau prix
                $savour_price = new SavourPrice();
                $savour_price->setPrice($params['savour-price']);
                $savour_price->setSavour($savour_list);
                $savour_price->setCake($cake);

                $entity_manager->persist($savour_price);
                $entity_manager->persist($savour_list);

            }

            // PRIX COULEUR
            if(isset($params['colour-submit'])){
                //Topping liste
                $colour_list = new ColourList();
                $colour_list->setLibelle($params['colour-libelle']);
                $colour_list->setHexa($params['colour-hexa']);
                $colour_list->setUser($this->getUser());

                //On ajoute un nouveau prix
                $colour_price = new ColourPrice();
                $colour_price->setPrice($params['colour-price']);
                $colour_price->setColour($colour_list);
                $colour_price->setCake($cake);

                $entity_manager->persist($colour_price);
                $entity_manager->persist($colour_list);
            }

            // PRIX NAPPAGE
            if(isset($params['supplement-submit'])){
                //Topping liste
                $supplement_list = new SupplementList();
                $supplement_list->setLibelle($params['supplement-libelle']);
                $supplement_list->setUser($this->getUser());

                //On ajoute un nouveau prix
                $supplement_price = new SupplementPrice();
                $supplement_price->setPrice($params['supplement-price']);
                $supplement_price->setSupplement($supplement_list);
                $supplement_price->setCake($cake);

                $entity_manager->persist($supplement_price);
                $entity_manager->persist($supplement_list);

            }


            $entity_manager->flush();
        }

        return $this->redirectToRoute('backoffice_pastry_show_option', array('id' => $cake->getId()));
    }

    public function delPriceOption(Request $request){
        $entity_manager = $this->getDoctrine()->getManager();
        $cake_repo = $entity_manager->getRepository('App:Cake');
        $topping_price_repo = $entity_manager->getRepository('App:ToppingPrice');
        $topping_list_repo = $entity_manager->getRepository('App:ToppingList');
        $savour_price_repo = $entity_manager->getRepository('App:SavourPrice');
        $savour_list_repo = $entity_manager->getRepository('App:SavourList');
        $colour_price_repo = $entity_manager->getRepository('App:ColourPrice');
        $colour_list_repo = $entity_manager->getRepository('App:ColourList');
        $supplement_price_repo = $entity_manager->getRepository('App:SupplementPrice');
        $supplement_list_repo = $entity_manager->getRepository('App:SupplementList');
        $params = $request->request->all();

        $cake = $cake_repo->findOneBy(array(
            'id' => $params['cake-id'],
            'user' => $this->getUser()->getId()
        ));

        //Récupération du bon gâteau
        if(!is_null($cake)) {

            // PRIX NAPPAGE
            if(isset($params['topping-id'])){
                $topping_price = $topping_price_repo->findOneByTopping($params['topping-id']);
                $topping_list = $topping_list_repo->findOneById($params['topping-id']);

                //Si y a pas de prix
                if(!is_null($topping_price)){
                    //On supprime le prix
                    $entity_manager->remove($topping_price);
                }

                if(!is_null($topping_list->getUser())){
                    $entity_manager->remove($topping_list);
                }
            }

            // PRIX COULEUR
            if(isset($params['colour-id'])){
                $colour_price = $colour_price_repo->findOneByColour($params['colour-id']);
                $colour_list = $colour_list_repo->findOneById($params['colour-id']);

                //Si y a pas de prix
                if(!is_null($colour_price)){
                    //On supprime le prix
                    $entity_manager->remove($colour_price);
                }

                if(!is_null($colour_list->getUser())){
                    $entity_manager->remove($colour_list);
                }
            }

            // PRIX SAVEUR
            if(isset($params['savour-id'])) {
                $savour_price = $savour_price_repo->findOneBySavour($params['savour-id']);
                $savour_list = $savour_list_repo->findOneById($params['savour-id']);

                //Si y a pas de prix
                if(!is_null($savour_price)){
                    //On supprime le prix
                    $entity_manager->remove($savour_price);
                }

                if(!is_null($savour_list->getUser())){
                    $entity_manager->remove($savour_list);
                }
            }

            // PRIX SUPPLEMEMENT
            if(isset($params['supplement-id'])){
                $supplement_price = $supplement_price_repo->findOneBySupplement($params['supplement-id']);
                $supplement_list = $supplement_list_repo->findOneById($params['supplement-id']);

                //Si y a pas de prix
                if(!is_null($supplement_price)){
                    //On supprime le prix
                    $entity_manager->remove($supplement_price);
                }

                if(!is_null($supplement_list->getUser())){
                    $entity_manager->remove($supplement_list);
                }
            }

            $entity_manager->flush();
        }

        return $this->redirectToRoute('backoffice_pastry_show_option', array('id' => $cake->getId()));
    }

//ADMIN//
    public function showAdminCake()
    {
        $entity_manager = $this->getDoctrine()->getManager();
        $pastry_speciality_repo = $entity_manager->getRepository('App:PastryChefSpecialityList');
        $pastry_speciality = $pastry_speciality_repo->findByUser($this->getUser());
        $cake_repo = $entity_manager->getRepository('App:Cake');
        if($this->getUser()->getUsername() == 'contact@hellomycake.com' || $this->getUser()->getUsername() == 'souhabenzakour@gmail.com'){
            $cakes = $cake_repo->findBy(array(
                'isDel' => 0
            ));

            $flag = 0;
            foreach ($pastry_speciality as $speciality) {
                if($speciality->getEtat() == 1){
                    $flag = 1;
                    break;
                }
            }

            if($flag == 0){
                return $this->redirectToRoute('backoffice_pastry_chef_edit_profil', array('error' => 10));
            }

            return $this->render('backofficePastryChef/admin/show-cake.html.twig', array(
                'cakes' => $cakes
            ));
        }else{
            return $this->redirectToRoute('home');
        }

    }

    public function showAdminUser()
    {
        $entity_manager = $this->getDoctrine()->getManager();
        $user_repo = $entity_manager->getRepository('App:User');
        $users = $user_repo->findAll();
        if($this->getUser()->getUsername() == 'contact@hellomycake.com' || $this->getUser()->getUsername() == 'souhabenzakour@gmail.com'){

            return $this->render('backofficePastryChef/admin/show-user.html.twig', array(
                'users' => $users
            ));
        }else{
            return $this->redirectToRoute('home');
        }

    }

//SERVICE
    function checkDistanceClientRequest($users, $lng = null, $lat = null){
        $session = $this->get('session');
        $entity_manager = $this->getDoctrine()->getManager();
        $user_repo = $entity_manager->getRepository('App:User');
        $results = [];

        $distance_tab=[];
        foreach ($users as $key => $user) {
            if(!is_null($user->getLocalisation())){
                $pastry_localisation = explode('-', $user->getLocalisation());
                if(!is_null($lng) && !is_null($lat)){
                    $distance = $this->distanceGeoPoints($lng, $lat, $pastry_localisation[0], $pastry_localisation[1]);
                }else{
                    $distance = $this->distanceGeoPoints($session->get('lng'), $session->get('lat'), $pastry_localisation[0], $pastry_localisation[1]);
                }

                if($distance < 30){
                    $results[] = array(
                        'id' => $user->getId(),
                        'distance' => $distance,
                    );

                    $id[] = $user->getId();
                    $distance_tab[$key] = $distance;
                }
            }
        }

        if(!empty($distance_tab)){
            array_multisort($distance_tab, SORT_ASC, $results);
        }

        $users_final = [];
        foreach ($results as $result) {
            $users_final[] = $user_repo->findOneById($result['id']);
        }

        return $users_final;
    }

    function distanceGeoPoints ($lat1, $lng1, $lat2, $lng2) {

        $earthRadius = 3958.75;

        $dLat = deg2rad($lat2-$lat1);
        $dLng = deg2rad($lng2-$lng1);

        $a = sin($dLat/2) * sin($dLat/2) +
            cos(deg2rad($lat1)) * cos(deg2rad($lat2)) *
            sin($dLng/2) * sin($dLng/2);
        $c = 2 * atan2(sqrt($a), sqrt(1-$a));
        $dist = $earthRadius * $c;

        // from miles
        $meterConversion = 1609;
        $geopointDistance = ($dist * $meterConversion)/1000;


        return round($geopointDistance, 2);
    }

    public function t($test){
        dump($test);
        die();
    }

    public function v($test){
        dump($test);
    }
}
