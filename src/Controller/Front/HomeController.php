<?php

/**
 * Created by PhpStorm.
 * User: Djilan
 * Date: 29/09/2019
 * Time: 09:40
 */

namespace App\Controller\Front;
use App\Entity\Order;
use App\Entity\OrderCake;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\ClientRequest;
use App\Form\ClientRequestType;
use Intervention\Image\ImageManagerStatic as Image;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;

class HomeController extends AbstractController
{
    private $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function showHome()
    {
        $session = $this->get('session');

        //SI LA PERSONNE EST CONNECTER ON VA LA REDIRIGER SINON PAS BESOIN
        if($this->getUser()){
            if(is_null($session->get('first-connection-pastry'))){
                $session->set('first-connection-pastry', false);
            }
            if(in_array('ROLE_PASTRY_CHEF',$this->getUser()->getRoles()) && $session->get('first-connection-pastry') == false){
                $session->set('first-connection-pastry', true);
                return $this->redirectToRoute('backoffice_pastry_chef_edit_profil');
            }
            if($session->get('before-checkout')){
                $session->set('before-checkout', false);

            }else{
                return $this->render('home.html.twig');
            }
        }
        return $this->render('home.html.twig');
    }

    public function cgu()
    {
        return $this->render('cgu.html.twig');
    }

    public function showPastryChefList(Request $request, $speciality)
    {
        $entity_manager = $this->getDoctrine()->getManager();
        $user_repo = $entity_manager->getRepository('App:User');
        $speciality_list_repo = $entity_manager->getRepository('App:SpecialityList');
        $session = $this->get('session');

        if ($speciality == '0') {
            $params = $request->request->all();
            if(isset($params['localisation'])){
                $localisation = htmlentities($params['localisation']);
                $localisation = explode('-', $localisation);

                $lng = $localisation[0];
                $lat = $localisation[1];

                $session->set('lng', $lng);
                $session->set('lat', $lat);
            }

            $users = $user_repo->getPastryChef();

            $users_final = $this->getSortUser($users);

            $user_session_id = [];

            foreach ($users_final as $user) {
                $user_session_id[] = $user->getId();
            }

            $session->set('user_final', $user_session_id);


        }
        else {

            $users = $session->get('user_final');
            $tab_users = [];
            foreach ($users as $user_id) {
                $tab_users[] = $user_repo->getOnePastryChef($user_id);
            }

            foreach ($tab_users as $key => $tab_user) {
                $flag = 0;
                foreach ($tab_user->getSpecialities() as $speciality_user) {
                    if ((intval($speciality) == $speciality_user->getSpeciality()->getId()) && $speciality_user->getEtat() == 1) {
                        $flag = 1;
                    }
                }
                if ($flag == 0) {
                    unset($tab_users[$key]);
                }
            }

            $users_final = [];

            if (!empty($tab_users)) {
                $users_final = $this->getSortUser($tab_users);
            }

        }

        $speciality_list = $speciality_list_repo->findAll();

        return $this->render('pastry-chef-list.html.twig', array(
            'users' => $users_final,
            'speciality_list' => $speciality_list
        ));
    }

    public function showCakeList($id)
    {
        $entity_manager = $this->getDoctrine()->getManager();
        $cake_repo = $entity_manager->getRepository('App:Cake');
        $user_repo = $entity_manager->getRepository('App:User');
        $pastr_chef_speciality_list_repo = $entity_manager->getRepository('App:PastryChefSpecialityList');
        $cakes = $cake_repo->findBy(array(
            'user' => $id,
            'active' => 1,
            'isDel' => 0,
        ));

        $user = $user_repo->getOnePastryChef($id);
        if(is_null($user)){
            return $this->redirectToRoute('home');
        }

        $speciality_list = $pastr_chef_speciality_list_repo->findByUser($id);

        return $this->render('cake-list.html.twig', array(
            'cakes' => $cakes,
            'speciality_list' => $speciality_list,
            'user' => $user
        ));
    }

    public function showCake(Request $request, $id)
    {
        $entity_manager = $this->getDoctrine()->getManager();
        $cake_repo = $entity_manager->getRepository('App:Cake');
        $cake_price_repo = $entity_manager->getRepository('App:CakePrice');
        $cake_size_repo = $entity_manager->getRepository('App:CakeSize');
        $user_repo = $entity_manager->getRepository('App:User');
        $cake = $cake_repo->findOneBy(array(
            'id' => $id,
            'active' => 1,
            'isDel' => 0
        ));
        $session = $this->get('session');

        $cake_size = $cake_size_repo->findAll();
        $session->set('price-id', null);

        if (!is_null($cake)) {

            $user = $user_repo->getOnePastryChef($cake->getUser()->getId());
            if(is_null($user)){
                return $this->redirectToRoute('home');
            }

            if($cake->getTypePrix() == 1){
                $cake_price = $cake_price_repo->findBy(array(
                    'cake' => $cake,
                    'size' => null
                ));
            }
            else{
                $cake_price = $cake_price_repo->findBy(array(
                    'cake' => $cake
                ));
                foreach ($cake_price as $key => $price) {
                    if(is_null($price->getSize())){
                        unset($cake_price[$key]);
                    }
                }
            }
            $firstPrice = null;
            $cake_price = array_values($cake_price);
            $flag = 0;
            foreach ($cake_price as $key => $row) {
                if(is_null($row->getSize())){
                    $flag = 1;
                }
            }
            if($flag == 0){
                foreach ($cake_price as $key => $row) {
                    $libelle[$key]  = $row->getSize()->getLibelle();
                }
                array_multisort($libelle, SORT_ASC, $cake_price);
            }

            if(!empty($cake_price)){
                if($cake->getTypePrix() == 1){
                    $firstPrice = $cake_price[0]->getQuantityMin() * $cake_price[0]->getPrice();
                }else{
                    $firstPrice = $cake_price[0]->getPrice();
                }
            }

            return $this->render('cake.html.twig', array(
                'cake_default' => $cake,
                'cake_price' => $cake_price,
                'firstPrice' => $firstPrice,
                'cake_size' => $cake_size
            ));

        } else {
            return $this->redirectToRoute('home');
        }

    }

    public function showOptions(Request $request)
    {
        //Récupération des paramètres
        $params = $request->request->all();
        $entity_manager = $this->getDoctrine()->getManager();
        $cake_repo = $entity_manager->getRepository('App:Cake');
        $cake_price_repo = $entity_manager->getRepository('App:CakePrice');
        $topping_price_repo = $entity_manager->getRepository('App:ToppingPrice');
        $topping_list_repo = $entity_manager->getRepository('App:ToppingList');
        $topping_active_repo = $entity_manager->getRepository('App:ToppingActive');
        $savour_price_repo = $entity_manager->getRepository('App:SavourPrice');
        $savour_list_repo = $entity_manager->getRepository('App:SavourList');
        $savour_active_repo = $entity_manager->getRepository('App:SavourActive');
        $colour_price_repo = $entity_manager->getRepository('App:ColourPrice');
        $colour_active_repo = $entity_manager->getRepository('App:ColourActive');
        $colour_list_repo = $entity_manager->getRepository('App:ColourList');
        $supplement_price_repo = $entity_manager->getRepository('App:SupplementPrice');
        $supplement_list_repo = $entity_manager->getRepository('App:SupplementList');
        $supplement_active_repo = $entity_manager->getRepository('App:SupplementActive');
        $dummy_stage_option_repo = $entity_manager->getRepository('App:DummyStageOption');
        $candle_option_repo = $entity_manager->getRepository('App:CandleOption');
        $text_option_repo = $entity_manager->getRepository('App:TextOption');
        $session = $this->get('session');
        $cake_id = $session->get('cake-id');
        $cake = $cake_repo->findOneBy(array(
            'id' => $cake_id,
            'isDel' => 0
        ));
        $redirect = 0;

        //Récupération des options
        //Prendre ceux de la liste et viré ceux qui ne sont pas activé
        //Récupérer également le prix
        //Il faut former un tableau contenant le l'id, le libelle et le tarif de l'option

        if(!is_null($cake) && $cake->getUser()->getIsDel() == 0 && $cake->getUser()->getActive() == 1){

            if(!is_null($session->get('size-id-quantity'))){
                $cake_price = $cake_price_repo->findOneBy(array(
                    'cake' =>  $cake,
                    'size' => null
                ));
            }else{
                $cake_price = $cake_price_repo->findOneBy(array(
                    'cake' =>  $cake,
                    'id' => $session->get('cake_price')->getId()
                ));
            }

            $colour_active = $colour_active_repo->findOneByCake($cake);
            $colours_final = [];
            if(!is_null($colour_active)) {
                $colours_list = explode(',', $colour_active->getColourList());
                foreach ($colours_list as $key => $colour_id) {
                    if(!is_null($colour_list_repo->findOneById($colour_id)) && !is_null($colour_price_repo->findOneBy(array('colour' => $colour_id, 'cake' => $cake)))){
                        $colours_final[$key]['colour'] = $colour_list_repo->findOneById($colour_id);
                        $colours_final[$key]['price'] = $colour_price_repo->findOneBy(array('colour' => $colour_id, 'cake' => $cake));
                        $redirect = 1;
                    }
                }
            }

            $savour_active = $savour_active_repo->findOneByCake($cake);
            $savours_final = [];
            if(!is_null($savour_active)){
                $savours_list = explode(',', $savour_active->getSavourList());
                foreach ($savours_list as $key => $savour_id) {
                    if (!is_null($savour_list_repo->findOneById($savour_id)) && !is_null($savour_price_repo->findOneBy(array('savour' => $savour_id, 'cake' => $cake)))) {
                        $savours_final[$key]['savour'] = $savour_list_repo->findOneById($savour_id);
                        $savours_final[$key]['price'] = $savour_price_repo->findOneBy(array('savour' => $savour_id, 'cake' => $cake));
                        $redirect = 1;
                    }
                }
            }

            $topping_active = $topping_active_repo->findOneByCake($cake);
            $toppings_final = [];
            if(!is_null($topping_active)) {
                $toppings_list = explode(',', $topping_active->getToppingList());
                foreach ($toppings_list as $key => $topping_id) {
                    if (!is_null($topping_list_repo->findOneById($topping_id)) && !is_null($topping_price_repo->findOneBy(array('topping' => $topping_id, 'cake' => $cake)))) {
                        $toppings_final[$key]['topping'] = $topping_list_repo->findOneById($topping_id);
                        $toppings_final[$key]['price'] = $topping_price_repo->findOneBy(array('topping' => $topping_id, 'cake' => $cake));
                        $redirect = 1;
                    }
                }
            }

            $supplement_active = $supplement_active_repo->findOneByCake($cake);
            $supplements_final = [];
            if(!is_null($supplement_active)) {
                $supplements_list = explode(',', $supplement_active->getSupplementList());
                foreach ($supplements_list as $key => $supplement_id) {
                    if (!is_null($supplement_list_repo->findOneById($supplement_id)) && !is_null($supplement_price_repo->findOneBy(array('supplement' => $supplement_id, 'cake' => $cake)))) {
                        $supplements_final[$key]['supplement'] = $supplement_list_repo->findOneById($supplement_id);
                        $supplements_final[$key]['price'] = $supplement_price_repo->findOneBy(array('supplement' => $supplement_id, 'cake' => $cake));
                        $redirect = 1;
                    }
                }
            }

            $text = $text_option_repo->findOneByCake($cake);
            if(!is_null($text)) $redirect = 1;
            $dummyStage = $dummy_stage_option_repo->findOneByCake($cake);
            if(!is_null($dummyStage)) $redirect = 1;
            $candle = $candle_option_repo->findOneByCake($cake);
            if(!is_null($candle)) $redirect = 1;

            if($redirect == 0){
                return $this->redirectToRoute('before_checkout');
            }

            return $this->render('options.html.twig', array(
                'cake' => $cake,
                'cake_price' => $cake_price,
                'colours_final' => $colours_final,
                'savours_final' => $savours_final,
                'toppings_final' => $toppings_final,
                'supplements_final' => $supplements_final,
                'inscription' => $text,
                'dummyStage' => $dummyStage,
                'candle' => $candle,
            ));

        }else{
            return $this->redirectToRoute('home');
        }
    }

    public function getCakePrice(Request $request)
    {
        $entity_manager = $this->getDoctrine()->getManager();
        $cake_repo = $entity_manager->getRepository('App:Cake');
        $cake_price_repo = $entity_manager->getRepository('App:CakePrice');

        $params = $request->request->all();

        $cake_id = $params['cake-id'];
        $price_id = $params['price-id'];
        $cake = $cake_repo->findOneBy(array(
            'id' => $cake_id,
            'isDel' => 0
        ));

        $cake_price = $cake_price_repo->findOneBy(array(
            'cake' => $cake,
            'id' => $price_id
        ));

        return new JsonResponse(['price' => $cake_price->getPrice()]);

    }

    public function beforeCheckout(Request $request){
        //Récupération des paramètres
        $params = $request->request->all();
        $session = $this->get('session');

        //Enregistrement des variables en session
        $session->set('date-retrait', (isset($params['date']) ? $params['date'] : null));
        $session->set('colour-option', (isset($params['colour-option']) ? $params['colour-option'] : null));
        $session->set('savour-option', (isset($params['savour-option']) ? $params['savour-option'] : null));
        $session->set('topping-option', (isset($params['topping-option']) ? $params['topping-option'] : null));
        $session->set('supplement-option', (isset($params['supplement-option']) ? $params['supplement-option'] : null));
        $session->set('dummy-stage-option', (isset($params['dummy-stage-option']) ? $params['dummy-stage-option'] : null));
        $session->set('candle-option', (isset($params['candle-option']) ? $params['candle-option'] : null));
        $session->set('text-option', (isset($params['text-option']) ? $params['text-option'] : null));
        $session->set('submit-option', (isset($params['submit-option']) ? $params['submit-option'] : null));

        if(!$this->getUser()) {
            $session->set('before-checkout', true);
        }

        return $this->redirectToRoute('show_checkout');

    }

    public function showCheckout(Request $request)
    {
        //Récupération des paramètres
        $params = $request->request->all();
        $entity_manager = $this->getDoctrine()->getManager();
        $cake_repo = $entity_manager->getRepository('App:Cake');
        $topping_price_repo = $entity_manager->getRepository('App:ToppingPrice');
        $topping_active_repo = $entity_manager->getRepository('App:ToppingActive');
        $savour_price_repo = $entity_manager->getRepository('App:SavourPrice');
        $savour_active_repo = $entity_manager->getRepository('App:SavourActive');
        $colour_price_repo = $entity_manager->getRepository('App:ColourPrice');
        $colour_active_repo = $entity_manager->getRepository('App:ColourActive');
        $supplement_price_repo = $entity_manager->getRepository('App:SupplementPrice');
        $supplement_active_repo = $entity_manager->getRepository('App:SupplementActive');
        $dummy_stage_option_repo = $entity_manager->getRepository('App:DummyStageOption');
        $candle_option_repo = $entity_manager->getRepository('App:CandleOption');
        $text_option_repo = $entity_manager->getRepository('App:TextOption');
        $cake_price_repo = $entity_manager->getRepository('App:CakePrice');
        $session = $this->get('session');
        $size_id_quantity = null;
        $price_id = null;
        if(!is_null($session->get('size-id-quantity'))){
            $size_id_quantity = $session->get('size-id-quantity');
        }else{
            $price_id = $session->get('cake_price')->getId();
            $session->set('price-id', $price_id);
        }

        $cake_id = $session->get('cake-id');
        $cake = $cake_repo->findOneBy(array(
            'id' => $cake_id,
            'isDel' => 0
        ));

        if(!is_null($cake)){
            if(!is_null($size_id_quantity)){
                $cake_price = $cake_price_repo->findOneBy(array(
                    'cake' =>  $cake,
                    'size' => null
                ));
                if($size_id_quantity > $cake_price->getQuantityMin()){
                    $session->set('size-id-quantity', $size_id_quantity);
                }else{
                    $size_id_quantity = $cake_price->getQuantityMin();
                    $session->set('size-id-quantity', $cake_price->getQuantityMin());
                }
            }else{
                $cake_price = $cake_price_repo->findOneBy(array(
                    'cake' =>  $cake,
                    'id' => $session->get('cake_price')->getId()
                ));
            }

            $options = [];
            $total = $session->get('temporary-price');
            if($session->get('finally-button') == false) {

                $colour_active = $colour_active_repo->findOneByCake($cake);
                if (!is_null($colour_active)) {
                    if(!is_null($session->get('colour-option')) && $session->get('colour-option') != ''){
                        $options['colour']['libelle'] = 'Couleur : '.$colour_price_repo->findOneBy(array('colour' => $session->get('colour-option'), 'cake' => $cake))->getColour()->getLibelle();
                        $options['colour']['prix'] = $colour_price_repo->findOneBy(array('colour' => $session->get('colour-option'), 'cake' => $cake))->getPrice();
                        $total+=$options['colour']['prix'];
                    }
                }

                $savour_active = $savour_active_repo->findOneByCake($cake);
                if (!is_null($savour_active)) {
                    if(!is_null($session->get('savour-option')) && $session->get('savour-option') != ''){
                        $options['savour']['libelle'] = 'Saveur : '.$savour_price_repo->findOneBy(array('savour' => $session->get('savour-option'), 'cake' => $cake))->getSavour()->getLibelle();
                        $options['savour']['prix'] = $savour_price_repo->findOneBy(array('savour' => $session->get('savour-option'), 'cake' => $cake))->getPrice();
                        $total+=$options['savour']['prix'];
                    }
                }

                $topping_active = $topping_active_repo->findOneByCake($cake);
                if (!is_null($topping_active)) {
                    if(!is_null($session->get('topping-option')) && $session->get('topping-option') != ''){
                        $options['topping']['libelle'] = 'Nappage : '.$topping_price_repo->findOneBy(array('topping' => $session->get('topping-option'), 'cake' => $cake))->getTopping()->getLibelle();
                        $options['topping']['prix'] = $topping_price_repo->findOneBy(array('topping' => $session->get('topping-option'), 'cake' => $cake))->getPrice();
                        $total+=$options['topping']['prix'];
                    }
                }

                $supplement_active = $supplement_active_repo->findOneByCake($cake);
                if (!is_null($supplement_active)) {
                    if(!is_null($session->get('supplement-option')) && $session->get('supplement-option') != ''){
                        $options['supplement']['libelle'] = 'Supplément : '.$supplement_price_repo->findOneBy(array('supplement' => $session->get('supplement-option'), 'cake' => $cake))->getSupplement()->getLibelle();
                        $options['supplement']['prix'] = $supplement_price_repo->findOneBy(array('supplement' => $session->get('supplement-option'), 'cake' => $cake))->getPrice();
                        $total+=$options['supplement']['prix'];
                    }
                }

                if(!is_null($session->get('text-option')) && $session->get('text-option') != ''){
                    $options['text']['libelle'] = 'Inscription : '.$session->get('text-option');
                    $options['text']['prix'] = $text_option_repo->findOneByCake($cake)->getPrice();
                    $total+=$options['text']['prix'];
                }

                if(!is_null($session->get('dummy-stage-option')) && $session->get('dummy-stage-option') != ''){
                    $options['dummy-stage']['libelle'] = 'Étage factice : '.$session->get('dummy-stage-option');
                    $options['dummy-stage']['prix'] = $dummy_stage_option_repo->findOneByCake($cake)->getPrice()*$session->get('dummy-stage-option');
                    $total+=$options['dummy-stage']['prix'];
                }

                if(!is_null($session->get('candle-option')) && $session->get('candle-option') != ''){
                    $options['candle-option']['libelle'] = 'Bougie : '.$session->get('candle-option');
                    $options['candle-option']['prix'] = $candle_option_repo->findOneByCake($cake)->getPrice()*$session->get('candle-option');
                    $total+=$options['candle-option']['prix'];
                }

                $session->set('options', $options);
            }

            $session->set('confirmation', 'ok');
            $session->set('total', $total);

        }else{
            return $this->redirectToRoute('home');
        }

        return $this->render('checkout.html.twig', array(
            'cake_price' => $cake_price,
            'size_id_quantity' => $size_id_quantity,
            'options' => $options,
            'total' => $total
        ));
    }

    public function payment(Request $request){
        $params = $request->request->all();
        $entity_manager = $this->getDoctrine()->getManager();
        $cake_repo = $entity_manager->getRepository('App:Cake');
        $cake_price_repo = $entity_manager->getRepository('App:CakePrice');
        $session = $this->get('session');

        //Récupération du cake
        $cake_id = $session->get('cake-id');
        $price_id = $session->get('price-id');

        //Récupération du cake
        $cake = $cake_repo->findOneBy(array(
            'id' => $cake_id,
            'isDel' => 0
        ));
        $today = new \DateTime();

        if($session->get('confirmation') == 'ok'){
            //Récupération de la date
            if(!empty($params['date'])){
                $date = str_replace('/','-',$params['date']);
                $order_date = new \DateTime($date);

                $duree_minimal = $cake->getDuration();
                $date_minimal = $today->modify('+'.$duree_minimal.' weeks');

                if($order_date < $date_minimal){
                    $order_date = $date_minimal;
                }

                //Il faut gérer les jours de disponibilités
                $dispo = $cake->getUser()->getDays();
                $order_day = $order_date->format('w');
                if(!in_array($order_day, $dispo)){
                    $check_day = $order_date;
                    while(!in_array($order_day, $dispo)){
                        $order_day++;
                        if($order_day >= 7){
                            $order_day = 0;
                        }
                        $check_day->modify('+ 1 day');
                    }
                    $order_date = $check_day;
                }

            }else{
                $duree_minimal = $cake->getDuration();
                $date_minimal = $today->modify('+'.$duree_minimal.' weeks');

                //Il faut gérer les jours de disponibilités
                $dispo = $cake->getUser()->getDays();
                $order_day = $date_minimal->format('w');
                if(!in_array($order_day, $dispo)){
                    $check_day = $date_minimal;
                    while(!in_array($order_day, $dispo)){
                        $order_day++;
                        if($order_day >= 7){
                            $order_day = 0;
                        }
                        $check_day->modify('+ 1 day');
                    }
                    $order_date = $check_day;
                }else{
                    $order_date = $date_minimal;
                }
            }

            if(!is_null($price_id)){
                $cake_price = $cake_price_repo->findOneBy(array(
                    'cake' =>  $cake,
                    'id' => $price_id
                ));
            }else{
                $cake_price = $cake_price_repo->findOneBy(array(
                    'cake' =>  $cake,
                    'size' => null
                ));
            }


            //Création du CODE
            $today_order = new \DateTime();
            $code = random_int(111111, 999999);
            $order = new Order();
            $order->setUser($this->getUser());
            $order->setDateCreation($today_order);
            $order->setCode($code);
            $order->setValidate(0);
            if(isset($params['info'])){
                $order->setInfo($params['info']);
            }else{
                $order->setInfo('');
            }
            $entity_manager->persist($order);
            $entity_manager->flush();

            $size_quantity = null;
            if(is_null($price_id)){
                $quantity = $session->get('size-id-quantity');
            }else{
                $quantity = $cake_price->getSize()->getLibelle();
            }

            $order_cake = new OrderCake();
            $order_cake->setCake($cake_price);
            $order_cake->setOrder($order);
            $order_cake->setUser($cake->getUser());
            $order_cake->setPrice($cake_price->getPrice());
            $order_cake->setTotal($session->get('total'));
            $order_cake->setQuantity($quantity);
            $order_cake->setOrderDate($order_date);
            $options = $session->get('options');
            $option_string = '';
            if(!is_null($options)){
                foreach ($options as $key => $option) {
                    if ($key == 'colour') {
                        $order_cake->setColour($option['libelle'] . '||' . $option['prix']);
                        $option_string .= '<p>'.$option['libelle'].' : '.$option['prix'].'€</p>';
                    }
                    if ($key == 'savour') {
                        $order_cake->setSavour($option['libelle'] . '||' . $option['prix']);
                        $option_string .= '<p>'.$option['libelle'].' : '.$option['prix'].'€</p>';
                    }
                    if ($key == 'topping') {
                        $order_cake->setTopping($option['libelle'] . '||' . $option['prix']);
                        $option_string .= '<p>'.$option['libelle'].' : '.$option['prix'].'€</p>';
                    }
                    if ($key == 'supplement') {
                        $order_cake->setSupplement($option['libelle'] . '||' . $option['prix']);
                        $option_string .= '<p>'.$option['libelle'].' : '.$option['prix'].'€</p>';
                    }
                    if ($key == 'text') {
                        $order_cake->setText($option['libelle'] . '||' . $option['prix']);
                        $option_string .= '<p>'.$option['libelle'].' : '.$option['prix'].'€</p>';
                    }
                    if ($key == 'candle-option') {
                        $order_cake->setCandle($option['libelle'] . '||' . $option['prix']);
                        $option_string .= '<p>'.$option['libelle'].' : '.$option['prix'].'€</p>';
                    }
                    if ($key == 'dummy-stage') {
                        $order_cake->setDummyStage($option['libelle'] . '||' . $option['prix']);
                        $option_string .= '<p>'.$option['libelle'].' : '.$option['prix'].'€</p>';
                    }
                }
            }

            $entity_manager->persist($order_cake);

            if(!is_null($cake_price->getCake()->getUser()->getEmail())){
                $this->mail(
                    $cake_price->getCake()->getUser()->getEmail(),
                    'Nouvelle commande | HelloMyCake',
                    'Nouvelle commande',
                    '<p>Un client vient de passer une commande et donc de payer l\'un de vos gâteaux ! Connectez-vous sur votre espace pâtissier pour la visualiser.</p>
                            <p>Attention ! Vous avez 24 heures pour nous confirmer la prise en charge de la commande.</p>'
                );
            }

            $entity_manager->flush();

            if($cake->getTypePatisserie() == 1){
                $type_patisserie = 'parts';
            }
            else{
                $type_patisserie = 'pièces';
            }
            if($quantity == 0){
                $size_quantity = $cake_price->getSize()->getibelle();
            }else{
                $size_quantity = $quantity;
            }

            $this->mail(
                $this->getUser()->getEmail(),
                'Confirmation de la commande | HelloMyCake',
                'Confirmation de la commande',
                '<div>Votre paiement a bien été réalisé.</div>
                         <h3 style="font-size: 30px">Code de retrait : '.$order->getCode().'</h3>
                         <p>Veuillez présenter le code de retrait au pâtissier lors de la réception de votre commande avant de récupérer votre gâteau !</p>
                         <h3 style="font-size: 30px">Récapitulatif de la commande</h3>
                         <p>Nom du gâteau : '.$cake->getName().'</p>
                         <p>Date de retrait : '.$order_date->format('d/m/Y').'</p>
                         <p>Prix du gâteau : '.$cake_price->getPrice().'€ ('.$size_quantity.' '.$type_patisserie.')</p>'.$option_string.'<p>Total : '.$session->get('total').'€</p>
                         <p>Information pour la commande : '.$order->getInfo().'</p>

                         <h3 style="font-size: 30px">Information du pâtissier</h3>
                         <p>Pâtissier : '.$cake->getUser()->getRaisonSociale().'</p>
                         <p>Adresse : '.$cake->getUser()->getAddress().' '.$cake->getUser()->getCity().'</p>
                         <p>Téléphone : '.$cake->getUser()->getPhone().'</p>
                         </br>
                         <div>Retrouver le récapitulatif de votre commmande sur votre espace client.</div>'
            );

            $this->mail(
                'contact@hellomycake.com',
                'Confirmation de la commande | HelloMyCake',
                'Confirmation de la commande',
                '<div>Votre paiement a bien été réalisé.</div>
                         <h3 style="font-size: 30px">Code de retrait : '.$order->getCode().'</h3>
                         <p>Veuillez présenter le code de retrait au pâtissier lors de la réception de votre commande avant de récupérer votre gâteau !</p>
                         <h3 style="font-size: 30px">Récapitulatif de la commande</h3>
                         <p>Nom du gâteau : '.$cake->getName().'</p>
                         <p>Date de retrait : '.$order_date->format('d/m/Y').'</p>
                         <p>Prix du gâteau : '.$cake_price->getPrice().'€ ('.$size_quantity.' '.$type_patisserie.')</p>'.$option_string.'<p>Total : '.$session->get('total').'€</p>

                         <h3 style="font-size: 30px">Information du pâtissier</h3>
                         <p>Pâtissier : '.$cake->getUser()->getRaisonSociale().'</p>
                         <p>Adresse : '.$cake->getUser()->getAddress().' '.$cake->getUser()->getCity().'</p>
                         <p>Téléphone : '.$cake->getUser()->getPhone().'</p>
                         </br>
                         <div>Retrouver le récapitulatif de votre commmande sur votre espace client.</div>'
            );

            $session->set('confirmation', 'no-ok');
            $session->remove('current_cake');
        }else{
            return $this->redirectToRoute('home');
        }

        return $this->render('confirmation.html.twig');

    }

    public function getTemporaryPriceQuantity(Request $request)
    {
        //Récupération des paramètres
        $params = $request->request->all();
        $entity_manager = $this->getDoctrine()->getManager();
        $cake_repo = $entity_manager->getRepository('App:Cake');
        $cake_price_repo = $entity_manager->getRepository('App:CakePrice');
        $session = $this->get('session');
        $size_id_quantity = null;
        $price_id = null;
        if(isset($params['size-id-quantity'])){
            $size_id_quantity = $params['size-id-quantity'];
        }else{
            $price_id = $params['price-id'];
            $session->set('price-id', $price_id);
        }
        $cake_id = $params['cake-id'];
        $session->set('cake-id', $cake_id);
        $cake = $cake_repo->findOneBy(array(
            'id' => $cake_id,
            'isDel' => 0
        ));

        if(!is_null($cake)){
            if(!is_null($size_id_quantity)){
                $cake_price = $cake_price_repo->findOneBy(array(
                    'cake' =>  $cake,
                    'size' => null
                ));
                if($size_id_quantity > $cake_price->getQuantityMin()){
                    $session->set('size-id-quantity', $size_id_quantity);
                }else{
                    $size_id_quantity = $cake_price->getQuantityMin();
                    $session->set('size-id-quantity', $cake_price->getQuantityMin());
                }

            }else{
                $cake_price = $cake_price_repo->findOneBy(array(
                    'cake' =>  $cake,
                    'id' => $price_id
                ));
            }

            if(!is_null($size_id_quantity)){
                $session->set('temporary-price', $cake_price->getPrice() * $size_id_quantity);
            }else{
                $session->set('temporary-price', $cake_price->getPrice());
            }

        }else{
            return $this->redirectToRoute('home');
        }
        $session->set('confirmation', 'ok');
        $session->set('cake_price', $cake_price);
        $session->set('user_days', $cake_price->getCake()->getUser()->getDays());

        if(!$this->getUser()) {
            $session->set('before-checkout', true);
        }
        if(isset($params['finally-button'])){
            $session->set('finally-button', true);
            return $this->redirectToRoute('show_checkout');
        }else{
            $session->set('finally-button', false);
        }

        return $this->redirectToRoute('show_options');
    }

    public function blogWeddingCake(){
        return $this->render('blog-wedding-cake.html.twig');
    }

    public function blogCakeDesign(){
        return $this->render('blog-cake-design.html.twig');
    }

    public function blogNumberCake(){
        return $this->render('blog-number-cake.html.twig');
    }

    public function blogBirthdayCake(){
        return $this->render('blog-birthday-cake.html.twig');
    }

    public function blogNightCake(){
        return $this->render('blog-night-cake.html.twig');
    }

    public function whyHelloMyCake(){
        return $this->render('why-hellomycake.html.twig');
    }

    public function clientRequestInformation(){
        return $this->render('client-request-information.html.twig');
    }

    public function clientRequestForm(Request $request){
        //TODO forcer le client à se connecter avant d'arriver dans le formulaire
        $entity_manager = $this->getDoctrine()->getManager();
        $user_repo = $entity_manager->getRepository('App:User');
        $ClientRequest = new ClientRequest();
        $form = $this->createForm(ClientRequestType::class, $ClientRequest);
        $success = false;

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $ClientRequest = $form->getData();

            /** @var UploadedFile $brochureFile */
            $photo = $form->get('photo')->getData();

            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($photo) {
                $originalFilename = pathinfo($photo->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$photo->guessExtension();


                // Move the file to the directory where photo are stored
                try {

                    $photo->move(
                        $this->getParameter('brochures_directory'),
                        $newFilename
                    );

                    //http://image.intervention.io/api/resize
                    $manager = Image::make($this->getParameter('brochures_directory').'/'.$newFilename);

                    // to finally create image instances
                    $manager->resize(300, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                    $manager->save($this->getParameter('brochures_directory').'/'.$newFilename);
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $params = $request->request->all();

                $ClientRequest->setPhoto($newFilename);
                $ClientRequest->setCoordinnates($params['coordonnates']);
                $ClientRequest->setUser($this->getUser());
                $ClientRequest->setCreationDate((new \DateTime()));
                $entity_manager->persist($ClientRequest);
                $entity_manager->flush();



                //Vérification des champs
                if(isset($params['coordonnates'])){
                    $localisation = htmlentities($params['coordonnates']);
                    $localisation = explode('-', $localisation);

                    $lng = $localisation[0];
                    $lat = $localisation[1];
                }

                $users = $user_repo->getPastryChef();

                $users_final = $this->getSortUser($users, $lng, $lat);

                //Insertions en base de données

                //Envoi de mail à tous les pâtissiers se trouvant à proximité avec le récap
                //TODO uploader l'image en taille réduite
                //TODO ajouter un lien au bouton
                if(strpos($_SERVER['HTTP_HOST'],'localhost') !== false) $url = $this->getParameter('url_dev');
                else $url = $this->getParameter('url_prod');

                foreach ($users_final as $user){
                    $this->mail(
                        $user->getEmail(),
                        'Un client souhaite passer commmande | HelloMyCake',
                        'Demande de gâteau',
                        '<div>Un client souhaite commander le gâteau avec les caractéristiques suivantes : </div>
                         <h3 style="font-size: 15px">Ville : '.$ClientRequest->getLocation().'</h3>
                         <h3 style="font-size: 15px">Évènement : '.$ClientRequest->getEvent().'</h3>
                         <h3 style="font-size: 15px">Nombre de personne : '.$ClientRequest->getNumberPerson().'</h3>
                         <h3 style="font-size: 15px">Informations supplémentaires (âge, prénom ...) : '.$ClientRequest->getInformations().'</h3>
                         <h3 style="font-size: 15px">Pour le : '.$ClientRequest->getDate()->format('d/m/Y').'</h3>
                         <h3>Voici la photo du gâteau souhaité : </h3>
                         <div><img width="300px" height="auto" src="'.$this->getParameter('brochures_directory').'/'.$newFilename.'" alt=""></div>
                         <a style="background-color: white;font-weight: bold;" class="btn" href="'.$url.'pastry/demande-gateau">La commande m\'intéresse</a>
                         <div>Retrouvez-nous sur Hello My Cake</div>'
                    );
                }
                //Envoi de mail au client le récap
                $this->mail(
                    $this->getUser()->getEmail(),
                    'Récapitulatif demande de gâteau | HelloMyCake',
                    'Demande de gâteau',
                    '<div>Bonjour, voici votre récapitulatif : </div>
                         <h3 style="font-size: 15px">Ville : '.$ClientRequest->getLocation().'</h3>
                         <h3 style="font-size: 15px">Évènement : '.$ClientRequest->getEvent().'</h3>
                         <h3 style="font-size: 15px">Nombre de personne : '.$ClientRequest->getNumberPerson().'</h3>
                         <h3 style="font-size: 15px">Informations supplémentaires (âge, prénom ...) : '.$ClientRequest->getInformations().'</h3>
                         <h3 style="font-size: 15px">Pour le : '.$ClientRequest->getDate()->format('d/m/Y').'</h3>
                         <h3>Voici la photo du gâteau souhaité : </h3>
                         <div><img width="300px" height="auto" src="'.$this->getParameter('brochures_directory').'/'.$newFilename.'" alt=""></div>'
                );

                //Envoi de mail au client le récap
                $this->mail(
                    'contact@hellomycake.com',
                    'Un client demande un gâteau | HelloMyCake',
                    'Demande de gâteau',
                    '<div>Bonjour, voici votre récapitulatif : </div>
                         <h3 style="font-size: 15px">Ville : '.$ClientRequest->getLocation().'</h3>
                         <h3 style="font-size: 15px">Évènement : '.$ClientRequest->getEvent().'</h3>
                         <h3 style="font-size: 15px">Nombre de personne : '.$ClientRequest->getNumberPerson().'</h3>
                         <h3 style="font-size: 15px">Informations supplémentaires (âge, prénom ...) : '.$ClientRequest->getInformations().'</h3>
                         <h3 style="font-size: 15px">Pour le : '.$ClientRequest->getDate()->format('d/m/Y').'</h3>
                         <h3>Voici la photo du gâteau souhaité : </h3>
                         <div><img width="300px" height="auto" src="'.$this->getParameter('brochures_directory').'/'.$newFilename.'" alt=""></div>'
                );

                $success = true;
            }

        }

        return $this->render('client-request-form.html.twig',[
            'form' => $form->createView(),
            'success' => $success
        ]);
    }

    function distanceGeoPoints ($lat1, $lng1, $lat2, $lng2) {

        $earthRadius = 3958.75;

        $dLat = deg2rad($lat2-$lat1);
        $dLng = deg2rad($lng2-$lng1);

        $a = sin($dLat/2) * sin($dLat/2) +
            cos(deg2rad($lat1)) * cos(deg2rad($lat2)) *
            sin($dLng/2) * sin($dLng/2);
        $c = 2 * atan2(sqrt($a), sqrt(1-$a));
        $dist = $earthRadius * $c;

        // from miles
        $meterConversion = 1609;
        $geopointDistance = ($dist * $meterConversion)/1000;


        return round($geopointDistance, 2);
    }

    function getSortUser($users, $lng = null, $lat = null){
        $session = $this->get('session');
        $entity_manager = $this->getDoctrine()->getManager();
        $user_repo = $entity_manager->getRepository('App:User');
        $results = [];

        $distance_tab=[];
        foreach ($users as $key => $user) {
            if(!is_null($user->getLocalisation())){
                $pastry_localisation = explode('-', $user->getLocalisation());
                if(!is_null($lng) && !is_null($lat)){
                    $distance = $this->distanceGeoPoints($lng, $lat, $pastry_localisation[0], $pastry_localisation[1]);
                }else{
                    $distance = $this->distanceGeoPoints($session->get('lng'), $session->get('lat'), $pastry_localisation[0], $pastry_localisation[1]);
                }

                if($distance < 30){
                    $results[] = array(
                        'id' => $user->getId(),
                        'distance' => $distance,
                    );

                    $id[] = $user->getId();
                    $distance_tab[$key] = $distance;
                }
            }
        }

        if(!empty($distance_tab)){
            array_multisort($distance_tab, SORT_ASC, $results);
        }

        $users_final = [];
        foreach ($results as $result) {
            $users_final[] = $user_repo->findOneById($result['id']);
        }

        return $users_final;
    }

    function mail($mail, $objet, $titre, $contain)
    {
        $message = (new \Swift_Message())
            ->setFrom('contact@hellomycake.com')
            ->setTo($mail)
            ->setSubject($objet);

        $img = $message->embed(\Swift_Image::fromPath('img/logo-new.png'));
        $message->setBody(
            $this->renderView(
            // templates/emails/registration.html.twig
                'mail.html.twig',
                [
                    'img' => $img,
                    'titre' => $titre,
                    'message' => $contain,
                ]
            ),
            'text/html'
        );

        $this->mailer->send($message);
    }

    public function review($id, $order_id, $hash){
        $entity_manager = $this->getDoctrine()->getManager();

        $order_repo = $entity_manager->getRepository('App:Order');
        $order = $order_repo->findOneBy([
            'id' => $order_id,
            'user' => $id
        ]);

        if(!is_null($order)){
            $hash_check = hash('ripemd160', $order->getUser()->getUsername().'hellomycakesecurity'.$order->getId());
            //Verification du hashage
            //Hachage ok ? Affichage du formulaire
            //Hachage non ok ? Affichage de la même pas avec un message d'erreur
            if($hash == $hash_check){
                return $this->render('review.html.twig');
            }else{
                return $this->redirectToRoute('home');
            }
        }
        //Nous sommes ici dès lors que le particulier à cliquer sur le bouton du mail
        //Nous recevrons un hachage accompagné de l'ID du client


        $this->t($hash);

        return $this->render('pastry-chef-list.html.twig');
    }

    public function t($test){
        dump($test);
        die();
    }

    public function v($test){
        dump($test);
    }
}