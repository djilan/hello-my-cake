<?php

/**

 * Created by PhpStorm.

 * User: Djilan

 * Date: 30/09/2019

 * Time: 19:29

 */

namespace App\Controller;

use App\Entity\PastryChefSpecialityList;

use App\Entity\User;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    private $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function login(Request $request, AuthenticationUtils $authenticationUtils)

    {
        $session = $this->get('session');
        $session->set('connexion', 'in');
        // get the login error if there is one

        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }

    public function loginPastryChef(Request $request, AuthenticationUtils $authenticationUtils)
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
//        highlight_string(var_export($authenticationUtils));
//        die();

        return $this->render('security/login-pastry-chef.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }

    public function showSignupPage()
    {
        return $this->render('security/signup.html.twig');
    }

    public function signUpRegister(Request $request, UserPasswordEncoderInterface $encoder)

    {
        //Stockage temporaire des données venant du formulaire
        $form_data = $request->request->all();
        //Check sur l'email, si il est pas, on le redirige vers la même page avec un message d'erreur
        //TODO Rajouter la vérification si l'utilisateur existe en base de données
        if (!filter_var($form_data['email'], FILTER_VALIDATE_EMAIL)) {
//            Transmission du message d'erreur à la vue
            return $this->render('security/signup.html.twig', array(
                'error' => 'Votre email est invalide.'
            ));
        }else{

            //ENREGISTREMENT EN BDD
            $entity_manager = $this->getDoctrine()->getManager();
            $user_repo = $entity_manager->getRepository('App:User');
            $user_duplicate = $user_repo->findOneByEmail($form_data['email']);

            if(!is_null($user_duplicate)){
                return $this->render('security/signup.html.twig', array(
                    'error' => 'Le compte est existant.'
                ));
            }

            $user = new User();

            $user->setType(0);
            $user->setUsername($form_data['email']);
            $user->setName($form_data['name']);
            $user->setFirstName($form_data['firstName']);
            $user->setEmail($form_data['email']);
            $user->setPhone($form_data['phone']);
            $encoded = $encoder->encodePassword($user, $form_data['password']);
            $user->setPassword($encoded);
            $user->setRoles(array('ROLE_USER'));
            $user->setActive(1);
            $user->setIsDel(0);

            $entity_manager->persist($user);
            $entity_manager->flush();

            $this->mailSignup(
                $form_data['email'],
                'Inscription | HelloMyCake',
                'Inscription client',
                'Bienvenue sur HelloMyCake ! Nous vous remercions de votre confiance. Pour finaliser la création de votre compte sur HelloMyCake.fr, vous devez confirmer votre adresse email en cliquant sur le lien ci-dessous et en saisissant votre mot de passe sur la page suivante.'
            );

            return $this->redirectToRoute('login');
        }
        return $this->render('security/signup.html.twig');

    }

    public function signUpPastryChefRegister (Request $request, UserPasswordEncoderInterface $encoder)
    {
        //Stockage temporaire des données venant du formulaire
        $form_data = $request->request->all();
        //Check sur l'email, si il est pas, on le redirige vers la même page avec un message d'erreur

        //TODO Rajouter la vérification si l'utilisateur existe en base de données

        if (!filter_var($form_data['email'], FILTER_VALIDATE_EMAIL)) {
//            Transmission du message d'erreur à la vue
            return $this->render('security/signup.html.twig', array(
                'error' => 'Votre email est invalide.'
            ));
        }
        //ENREGISTREMENT EN BDD

        $entity_manager = $this->getDoctrine()->getManager();
        $user_repo = $entity_manager->getRepository('App:User');
        $speciality_list_repo = $entity_manager->getRepository('App:SpecialityList');
        $user_duplicate = $user_repo->findOneBy(array('email' => $form_data['email'], 'username' => $form_data['email']));

        if(!is_null($user_duplicate)){
            return $this->render('security/signup-pastry-chef.html.twig', array(
                'error' => 'Le compte est existant.'
            ));
        }

        $user = new User();
        $user->setType(1);
        $user->setUsername($form_data['email']);
        $user->setRaisonSociale($form_data['company-name']);
        $user->setName($form_data['name']);
        $user->setFirstName($form_data['firstName']);
        $user->setEmail($form_data['email']);
        $user->setPhone($form_data['phone']);
        $user->setAddress('');
//        $user->setZipCode($form_data['zipCode']);
        $user->setCity('');
        $user->setLocalisation('');
        $encoded = $encoder->encodePassword($user, $form_data['password']);
        $user->setPassword($encoded);
        $user->setDays(array(1 =>"1", 2 =>"2", 3 =>"3", 4 =>"4", 5 =>"5", 6 =>"6"));
        $user->setRoles(array('ROLE_PASTRY_CHEF'));
        $user->setIsDel(1);
        $user->setActive(1);

        $entity_manager->persist($user);
        $entity_manager->flush();

        //CREATION DES SPECIALITES
        $speciality_list = $speciality_list_repo->findAll();

        foreach ($speciality_list as $speciality) {
            $pastry_speciality = new PastryChefSpecialityList();
            $pastry_speciality->setUser($user);
            $pastry_speciality->setSpeciality($speciality);
            $pastry_speciality->setEtat(0);
            $entity_manager->persist($pastry_speciality);
        }
        $entity_manager->flush();

        $this->mailSignupPastry(
            $form_data['email'],
            'Inscription | HelloMyCake',
            'Inscription pâtissier',
            'Bienvenue sur HelloMyCake ! Nous sommes très heureux de vous compter parmis nous ! Nous reviendrons vers vous prochainement pour finaliser votre inscription.'
        );

        $this->mail(
            'contact@hellomycake.com',
            'Nouveau pâtissier | HelloMyCake',
            'Inscription Nouveau Pâtissier',
            'Mail : '. $form_data['email']. ' - Raison Social : '. $form_data['company-name']);

        $this->mail(
            'souhabenzakour@gmail.com',
            'Nouveau pâtissier | HelloMyCake',
            'Inscription Nouveau Pâtissier',
            'Mail : '. $form_data['email']. ' - Raison Social : '. $form_data['company-name']);

        return $this->render('security/after-signup.html.twig', array(
            'success_password' => 'Inscription réussie ! Vous pouvez dès à présent vous connecter.'
        ));
    }

    public function signupPastryChef (Request $request){
        return $this->render('security/signup-pastry-chef.html.twig');
    }

    public function signupPrePastryChef (){
        return $this->render('pastry-pre-inscription.html.twig');
    }

    public function logout(){

    }

    public function forgotPassword(Request $request, UserPasswordEncoderInterface $encoder){
        $params = $request->request->all();
        if(empty($params)){
            return $this->render('security/forgot-password.html.twig');
        }else{
            $entity_manager = $this->getDoctrine()->getManager();
            $user_repo = $entity_manager->getRepository('App:User');
            $user = $user_repo->findOneByUsername($params['email']);

            if(is_null($user)){
                return $this->render('security/forgot-password.html.twig', array(
                    'error' => 'Le compte n\'existe pas'
                ));
            }else{
                //Génération du nouveau mot de passe
                $new_password = substr(str_shuffle(MD5(microtime())), 0, 10);
                $encoded = $encoder->encodePassword($user, $new_password);
                $user->setPassword($encoded);
                $entity_manager->persist($user);
                $entity_manager->flush();
                //Envoi du mail mot de passe oublié
                $this->mail(
                    $params['email'],
                    'Réinitialisation du mot de passe | HelloMyCake',
                    'Réinitialisation du mot de passe',
                    'Bonjour, votre nouveau mot de passe est : '.$new_password.'. Ce dernier est modifiable depuis votre espace client.'
                );
                return $this->render('security/login.html.twig', array(
                    'success_password' => 'Un email avec un nouveau mot de passe vient de vous être envoyé.'
                ));
            }
        }
    }

    function mailSignup($mail, $objet, $titre, $contain)
    {
        $message = (new \Swift_Message('Hello Email'))
            ->setFrom('contact@hellomycake.com')
            ->setTo($mail)
            ->setSubject($objet);
        $img = $message->embed(\Swift_Image::fromPath('img/logo-new.png'));
        $message->setBody(
            $this->renderView(
            // templates/emails/registration.html.twig
                'mail-signup.html.twig',
                [
                    'img' => $img,
                    'titre' => $titre,
                    'message' => $contain,
                ]
            ),
            'text/html'
        );
        $this->mailer->send($message);
    }

    function mailSignupPastry($mail, $objet, $titre, $contain)
    {
        $message = (new \Swift_Message('Hello Email'))
            ->setFrom('contact@hellomycake.com')
            ->setTo($mail)
            ->setSubject($objet);
        $img = $message->embed(\Swift_Image::fromPath('img/logo-new.png'));
        $message->setBody(
            $this->renderView(
            // templates/emails/registration.html.twig
                'mail-signup-pastry.html.twig',
                [
                    'img' => $img,
                    'titre' => $titre,
                    'message' => $contain,
                ]
            ),
            'text/html'
        );
        $this->mailer->send($message);
    }

    function mailForgotPassword($mail, $objet, $titre, $contain)
    {
        $message = (new \Swift_Message('Hello Email'))
            ->setFrom('contact@hellomycake.com')
            ->setTo($mail)
            ->setSubject($objet);

        $img = $message->embed(\Swift_Image::fromPath('img/logo-new.png'));
        $message->setBody(
            $this->renderView(
            // templates/emails/registration.html.twig
                'mail.html.twig',
                [
                    'img' => $img,
                    'titre' => $titre,
                    'message' => $contain,
                ]
            ),
            'text/html'
        );
        $this->mailer->send($message);
    }

    function mail($mail, $objet, $titre, $contain)
    {
        $message = (new \Swift_Message())
            ->setFrom('contact@hellomycake.com')
            ->setTo($mail)
            ->setSubject($objet);

        $img = $message->embed(\Swift_Image::fromPath('img/logo-new.png'));
        $message->setBody(
            $this->renderView(
            // templates/emails/registration.html.twig
                'mail.html.twig',
                [
                    'img' => $img,
                    'titre' => $titre,
                    'message' => $contain,
                ]
            ),
            'text/html'
        );

        $this->mailer->send($message);
    }

    public function t($test){
        dump($test);
        die();
    }

    public function v($test){
        dump($test);
    }
}
