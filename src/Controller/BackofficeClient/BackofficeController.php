<?php
/**
 * Created by PhpStorm.
 * User: Djilan
 * Date: 29/09/2019
 * Time: 09:40
 */

namespace App\Controller\BackofficeClient;
use App\Entity\ClientRequest;
use App\Entity\ClientRequestMessage;
use App\Entity\ClientRequestProposal;
use App\Entity\User;
use App\Form\ClientRequestMessageType;
use Intervention\Image\ImageManagerStatic as Image;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class BackofficeController extends AbstractController
{
    public function editProfil(Request $request, UserPasswordEncoderInterface $encoder, $error)
    {
        $form_data = $request->request->all();
        $entity_manager = $this->getDoctrine()->getManager();
        $user_repo = $entity_manager->getRepository('App:User');
        $user = $user_repo->findOneById($this->getUser()->getId());
        if(!empty($form_data)){
            if(!empty($form_data['old_password'])){
                if($encoder->isPasswordValid($user, $form_data['old_password'])) {
                    if(empty($form_data['new_password'])){
                        return $this->render('backofficeClient/edit-profil.html.twig', array(
                            'user' => $user,
                            'error' => 3
                        ));
                    }elseif(strlen($form_data['new_password']) <= 6){
                        return $this->render('backofficeClient/edit-profil.html.twig', array(
                            'user' => $user,
                            'error' => 2
                        ));
                    }
                    $encoded = $encoder->encodePassword($user, $form_data['new_password']);
                    $user->setPassword($encoded);
                }else{
                    return $this->render('backofficeClient/edit-profil.html.twig', array(
                        'user' => $user,
                        'error' => 1
                    ));
                }
            }
            $user->setName($form_data['name']);
            $user->setFirstName($form_data['firstName']);
            $user->setPhone($form_data['phone']);
            $entity_manager->persist($user);
            $entity_manager->flush();

            $error = 20;

        }

        return $this->render('backofficeClient/edit-profil.html.twig', array(
            'user' => $user,
            'error' => $error
        ));
    }

    public function showOrder()
    {
        $entity_manager= $this->getDoctrine()->getManager();
        $order_repo = $entity_manager->getRepository('App:Order');

        $orders = $order_repo->findByUser($this->getUser());

        foreach ($orders as &$order) {
            $dummy_stage_tab = explode('||', $order->getOrderCake()->getDummyStage());
            $colour_tab = explode('||', $order->getOrderCake()->getColour());
            $savour_tab = explode('||', $order->getOrderCake()->getSavour());
            $topping_tab = explode('||', $order->getOrderCake()->getTopping());
            $text_tab = explode('||', $order->getOrderCake()->getText());
            $supplement_tab = explode('||', $order->getOrderCake()->getSupplement());
            $candle_tab = explode('||', $order->getOrderCake()->getCandle());
            if(count($dummy_stage_tab) > 1) $order->getOrderCake()->setDummyStage($dummy_stage_tab[0].' - '.$dummy_stage_tab[1]);
            if(count($colour_tab) > 1) $order->getOrderCake()->setColour($colour_tab[0].' - '.$colour_tab[1]);
            if(count($savour_tab) > 1) $order->getOrderCake()->setSavour($savour_tab[0].' - '.$savour_tab[1]);
            if(count($topping_tab) > 1) $order->getOrderCake()->setTopping($topping_tab[0].' - '.$topping_tab[1]);
            if(count($text_tab) > 1) $order->getOrderCake()->setText($text_tab[0].' - '.$text_tab[1]);
            if(count($supplement_tab) > 1) $order->getOrderCake()->setSupplement($supplement_tab[0].' - '.$supplement_tab[1]);
            if(count($candle_tab) > 1) $order->getOrderCake()->setCandle($candle_tab[0].' - '.$candle_tab[1]);
        }

        return $this->render('backofficeClient/show-order.html.twig',array(
            'orders' => $orders
        ));
    }

    public function showClientRequest(){
        $entity_manager= $this->getDoctrine()->getManager();
        $client_request_repo = $entity_manager->getRepository('App:ClientRequest');
        $clients_request = $client_request_repo->findByUser($this->getUser());

        return $this->render('backofficeClient/show-client-request.html.twig',array(
            'clients_request' => $clients_request
        ));
    }

    public function getPastryChefForMessages(ClientRequest $clientRequest){
        $entity_manager= $this->getDoctrine()->getManager();
        $client_request_message_repo = $entity_manager->getRepository('App:ClientRequestMessage');
        $user_repo = $entity_manager->getRepository('App:User');
        $client_request_messages = $client_request_message_repo->findByUser($this->getUser());
        $pastry_chefs = [];
        $pastry_chefs_id = [];
        foreach ($client_request_messages as $client_request_message) {

            if(!in_array($client_request_message->getUserPastry()->getId(), $pastry_chefs_id)){
                $pastry_chefs[] = $user_repo->findOneById($client_request_message->getUserPastry()->getId());
                $pastry_chefs_id[] = $user_repo->findOneById($client_request_message->getUserPastry()->getId())->getId();
            }
        }

        return $this->render('backofficeClient/show-pastry-chef-for-client-messages.html.twig',array(
            'client_request' => $clientRequest,
            'pastry_chefs' => $pastry_chefs
        ));
    }

    public function showClientRequestInbox(ClientRequest $clientRequest, User $pastryChef, Request $request){

        $entity_manager= $this->getDoctrine()->getManager();
        $client_request_repo = $entity_manager->getRepository('App:ClientRequest');
        $client_request_proposal_repo = $entity_manager->getRepository('App:ClientRequestProposal');
        $client_request_proposal = $client_request_proposal_repo->findOneBy(['clientRequest' => $clientRequest, 'user' => $this->getUser(), 'userPastry' => $pastryChef], ['id' => 'DESC'], 1);

        $client_request_message = new ClientRequestMessage();
        $form = $this->createForm(ClientRequestMessageType::Class, $client_request_message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && !empty($_FILES)) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $client_request_message = $form->getData();

            $client_request_value = $client_request_repo->findOneById($clientRequest);
            $session = $this->get('session');
            if(!is_null($client_request_value) && $session->get('client_request') == $clientRequest->getId()){
                /** @var UploadedFile $brochureFile */
                $photo = $form->get('photo')->getData();

                // this condition is needed because the 'brochure' field is not required
                // so the PDF file must be processed only when a file is uploaded
                if ($photo) {
                    $originalFilename = pathinfo($photo->getClientOriginalName(), PATHINFO_FILENAME);
                    // this is needed to safely include the file name as part of the URL
                    $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                    $newFilename = $safeFilename . '-' . uniqid() . '.' . $photo->guessExtension();

                    // Move the file to the directory where photo are stored
                    try {

                        $photo->move(
                            $this->getParameter('photo_message_directory'),
                            $newFilename
                        );

                        //http://image.intervention.io/api/resize
                        $manager = Image::make($this->getParameter('photo_message_directory') . '/' . $newFilename);

                        // to finally create image instances
                        $manager->resize(150, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });

                        $manager->save($this->getParameter('photo_message_directory') . '/' . $newFilename);
                    } catch (FileException $e) {
                        // ... handle exception if something happens during file upload
                    }

                    // updates the 'brochureFilename' property to store the PDF file name
                    // instead of its contents

                    $date = new \DateTime('now');
                    $client_request_message = new ClientRequestMessage();
                    $client_request_message->setClientRequest($client_request_value);
                    $client_request_message->setCreationDate($date);
                    $client_request_message->setUser($client_request_value->getUser());
                    $client_request_message->setUserPastry($this->getUser());
                    $client_request_message->setPhoto($newFilename);
                    $entity_manager->persist($client_request_message);
                    $entity_manager->flush();
                    return $this->redirectToRoute("backoffice_pastry_chef_client_request_inbox",['clientRequest' => $clientRequest->getId()]);
                }

            }

        }
        else{
            $session = $this->get('session');
            $session->set('client_request', $clientRequest->getId());
        }

        return $this->render('backofficeClient/show-client-request-inbox.html.twig',array(
            'client_request' => $clientRequest,
            'pastry_chef' => $pastryChef,
            'client_request_proposal' => $client_request_proposal,
            'form' => $form->createView()
        ));
    }

    public function addMessageClientRequest(Request $request){

        $entity_manager= $this->getDoctrine()->getManager();
        $message = strip_tags($request->request->get('message'));
        $client_request = strip_tags($request->request->get('client_request'));
        $pastry_chef_id = strip_tags($request->request->get('pastry_chef'));
        $client_request_repo = $entity_manager->getRepository('App:clientRequest');
        $user_repo = $entity_manager->getRepository('App:User');
        $pastry_chef = $user_repo->findOneById($pastry_chef_id);
        $client_request_value = $client_request_repo->findOneById($client_request);
        $session = $this->get('session');
        if(!is_null($client_request_value) && $session->get('client_request') == $client_request){
            $date = new \DateTime('now');
            $client_request_message = new ClientRequestMessage();
            $client_request_message->setClientRequest($client_request_value);
            $client_request_message->setCreationDate($date);
            $client_request_message->setMessage($message);
            $client_request_message->setUserPastry($pastry_chef);
            $client_request_message->setUser($this->getUser());
            $client_request_message->setSender('client');

            $entity_manager->persist($client_request_message);
            $entity_manager->flush();
            $user_string = $this->getUser()->getRaisonSociale().' - '.$this->getUser()->getName().' '.$this->getUser()->getFirstName().' '.$date->format('d/m/Y'). ' '.$date->format('H:i:s');

            return new JsonResponse(['result' => true, 'message' => $message, 'user_string' => $user_string]);
        }else{
            return new JsonResponse(['result' => false]);
        }

    }

    public function getMessagesClientRequest(Request $request){

        $entity_manager= $this->getDoctrine()->getManager();
        $client_request = strip_tags($request->request->get('client_request'));
        $pastry_chef = strip_tags($request->request->get('pastry_chef'));
        $client_request_repo = $entity_manager->getRepository('App:clientRequest');
        $client_request_message_repo = $entity_manager->getRepository('App:clientRequestMessage');
        $client_request_value = $client_request_repo->findOneById($client_request);

        $session = $this->get('session');
        if(!is_null($client_request_value) && $session->get('client_request') == $client_request){
            $date = new \DateTime('now');
            $messages = $client_request_message_repo->findBy(['clientRequest' => $client_request, 'user' => $this->getUser(), 'userPastry' => $pastry_chef]);
            $message_final = '';
            foreach ($messages as $message) {
                if(!is_null($message->getMessage())){
                    $message_string = '<p>'.$message->getMessage().'</p>';
                }else{
                    if(strpos($_SERVER['HTTP_HOST'],'localhost') !== false) $url = $this->getParameter('url_dev');
                    else $url = $this->getParameter('url_prod');
                    $message_string = '<img width="150px" src="'.$url.'img/client_request/'.$message->getPhoto().'"/>';
                }
                $user_string = '<span>'.$this->getUser()->getRaisonSociale().' - '.$this->getUser()->getName().' '.$this->getUser()->getFirstName().' '.$message->getCreationDate()->format('d/m/Y'). ' '.$message->getCreationDate()->format('H:i:s').'</span>';
                $message_final .= $message_string.$user_string;
            }
            return new JsonResponse(['result' => true, 'messages' => $message_final, 'count' => count($messages)]);
        }else{
            return new JsonResponse(['result' => false]);
        }

    }

    public function addClientProposalClientRequest(Request $request){

        $entity_manager= $this->getDoctrine()->getManager();
        $client_request = strip_tags($request->request->get('client_request'));
        $price = strip_tags($request->request->get('proposeprice'));
        $client_request_repo = $entity_manager->getRepository('App:clientRequest');
        $client_request_value = $client_request_repo->findOneById($client_request);

        $session = $this->get('session');
        if(!is_null($client_request_value) && $session->get('client_request') == $client_request){

            $proposal = new ClientRequestProposal();
            $proposal->setUserPastry($this->getUser());
            $proposal->setUser($client_request_value->getUser());
            $proposal->setClientRequest($client_request_value);
            $proposal->setPrice($price);
            $proposal->setCreationDate((new \DateTime('now')));
            $entity_manager->persist($proposal);
            $entity_manager->flush();

            return $this->redirectToRoute('backoffice_pastry_chef_client_request_inbox', ['clientRequest' => $client_request_value->getId()]);

        }else{
            return $this->redirectToRoute('backoffice_pastry_chef_client_request');
        }
    }

    public function t($test){
        dump($test);
        die();
    }

    public function v($test){
        dump($test);
    }
}