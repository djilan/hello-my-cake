<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cake
 * @ORM\Table(name="review")
 * @ORM\Entity
 */
class Review
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */

    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cake")
     * @ORM\JoinColumn(name="cake_id", referencedColumnName="id")
     */
    private $cake;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $size;

    /**
     * @var integer
     * @ORM\Column(name="nb_stars", type="integer", length=2, nullable=false)
     */
    private $nbStars;

    /**
     * @var integer
     * @ORM\Column(name="contain", type="string", length=500, nullable=false)
     */
    private $contain;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    /**
     * @return mixed
     */
    public function getCake()
    {
        return $this->cake;
    }

    /**
     * @param mixed $cake
     */
    public function setCake($cake): void
    {
        $this->cake = $cake;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size): void
    {
        $this->size = $size;
    }

    /**
     * @return int
     */
    public function getNbStars(): int
    {
        return $this->nbStars;
    }

    /**
     * @param int $nbStars
     */
    public function setNbStars(int $nbStars): void
    {
        $this->nbStars = $nbStars;
    }

    /**
     * @return int
     */
    public function getContain(): int
    {
        return $this->contain;
    }

    /**
     * @param int $contain
     */
    public function setContain(int $contain): void
    {
        $this->contain = $contain;
    }



}
