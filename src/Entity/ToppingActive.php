<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cake
 *
 * @ORM\Table(name="topping_active")
 * @ORM\Entity
 */
class ToppingActive
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cake")
     * @ORM\JoinColumn(name="id_cake", referencedColumnName="id")
     */
    private $cake;

    /**
     * @var string
     *
     * @ORM\Column(name="topping_list", type="string", length=255, nullable=false)
     */
    private $toppingList;
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCake()
    {
        return $this->cake;
    }

    /**
     * @param mixed $cake
     */
    public function setCake($cake): void
    {
        $this->cake = $cake;
    }

    /**
     * @return string
     */
    public function getToppingList(): string
    {
        return $this->toppingList;
    }

    /**
     * @param string $toppingList
     */
    public function setToppingList(string $toppingList): void
    {
        $this->toppingList = $toppingList;
    }

}
