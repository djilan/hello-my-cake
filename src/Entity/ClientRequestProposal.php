<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRequestProposalRepository")
 */
class ClientRequestProposal
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\user", inversedBy="clientRequestProposals")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\user", inversedBy="clientRequestProposalPastry")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userPastry;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\clientRequest", inversedBy="clientRequestProposals")
     * @ORM\JoinColumn(nullable=false)
     */
    private $clientRequest;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creationDate;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $accept;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?user
    {
        return $this->user;
    }

    public function setUser(?user $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUserPastry(): ?user
    {
        return $this->userPastry;
    }

    public function setUserPastry(?user $userPastry): self
    {
        $this->userPastry = $userPastry;

        return $this;
    }

    public function getClientRequest(): ?clientRequest
    {
        return $this->clientRequest;
    }

    public function setClientRequest(?clientRequest $clientRequest): self
    {
        $this->clientRequest = $clientRequest;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getAccept(): ?bool
    {
        return $this->accept;
    }

    public function setAccept(?bool $accept): self
    {
        $this->accept = $accept;

        return $this;
    }
}
