<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cake
 *
 * @ORM\Table(name="order_cake")
 * @ORM\Entity
 */
class OrderCake
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CakePrice")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cake;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Order", inversedBy="cakes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $order;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", length=11, nullable=false)
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float", length=11, nullable=false)
     */
    private $total;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", length=11, nullable=false)
     */
    private $quantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_date", type="date")
     */
    private $orderDate;

    /**
     * @var string
     *
     * @ORM\Column(name="dummy_stage", type="string", length=255)
     */
    private $dummyStage;
    /**
     * @var string
     *
     * @ORM\Column(name="savour", type="string", length=255)
     */
    private $savour;
    /**
     * @var string
     *
     * @ORM\Column(name="supplement", type="string", length=255)
     */
    private $supplement;
    /**
     * @var string
     *
     * @ORM\Column(name="topping", type="string", length=255)
     */
    private $topping;
    /**
     * @var string
     *
     * @ORM\Column(name="colour", type="string", length=255)
     */
    private $colour;
    /**
     * @var string
     *
     * @ORM\Column(name="candle", type="string", length=255)
     */
    private $candle;
    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=255)
     */
    private $text;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCake()
    {
        return $this->cake;
    }

    /**
     * @param mixed $cake
     */
    public function setCake($cake): void
    {
        $this->cake = $cake;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order): void
    {
        $this->order = $order;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }


    /**
     * @return mixed
     */
    public function getCakeSize()
    {
        return $this->cakeSize;
    }

    /**
     * @param mixed $cake_size
     */
    public function setCakeSize($cake_size): void
    {
        $this->cakeSize = $cake_size;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * @param int $orderDate
     */
    public function setOrderDate($orderDate): void
    {
        $this->orderDate = $orderDate;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return string
     */
    public function getDummyStage()
    {
        return $this->dummyStage;
    }

    /**
     * @param string $dummyStage
     */
    public function setDummyStage(string $dummyStage): void
    {
        $this->dummyStage = $dummyStage;
    }

    /**
     * @return string
     */
    public function getSavour()
    {
        return $this->savour;
    }

    /**
     * @param string $savour
     */
    public function setSavour(string $savour): void
    {
        $this->savour = $savour;
    }

    /**
     * @return string
     */
    public function getSupplement()
    {
        return $this->supplement;
    }

    /**
     * @param string $supplement
     */
    public function setSupplement(string $supplement): void
    {
        $this->supplement = $supplement;
    }

    /**
     * @return string
     */
    public function getTopping()
    {
        return $this->topping;
    }

    /**
     * @param string $topping
     */
    public function setTopping(string $topping): void
    {
        $this->topping = $topping;
    }

    /**
     * @return string
     */
    public function getColour()
    {
        return $this->colour;
    }

    /**
     * @param string $colour
     */
    public function setColour(string $colour): void
    {
        $this->colour = $colour;
    }

    /**
     * @return string
     */
    public function getCandle()
    {
        return $this->candle;
    }

    /**
     * @param string $candle
     */
    public function setCandle(string $candle): void
    {
        $this->candle = $candle;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return int
     */
    public function getTotal(): float
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal(float $total): void
    {
        $this->total = $total;
    }

}
