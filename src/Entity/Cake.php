<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cake
 *
 * @ORM\Table(name="cake")
 * @ORM\Entity
 */
class Cake
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SpecialityList", inversedBy="specialities")
     * @ORM\JoinColumn(name="speciality_code", referencedColumnName="id")
     */
    private $speciality;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;
    /**
     * @var string
     *
     * @ORM\Column(name="ingredient", type="text", nullable=true)
     */
    private $ingredient;
    /**
     * @var array
     *
     * @ORM\Column(name="urlImage", type="array", nullable=false)
     */
    private $urlImage;
    /**
     * @var int
     *
     * @ORM\Column(name="duration", type="integer", nullable=false)
     */
    private $duration;
    /**
     * @var string
     *
     * @ORM\Column(name="day_week", type="string", length=50, nullable=true)
     */
    private $dayWeek;
    /**
     * @var string
     *
     * @ORM\Column(name="info_etage", type="string", length=255, nullable=true)
     */
    private $infoEtage;
    /**
     * @var int
     *
     * @ORM\Column(name="type_patisserie", type="integer", nullable=false)
     */
    private $typePatisserie;
    /**
     * @var int
     *
     * @ORM\Column(name="type_prix", type="integer", nullable=false)
     */
    private $typePrix;
    /**
     * @var int
     *
     * @ORM\Column(name="dummy_stage", type="integer")
     */
    private $dummyStage;
    /**
     * @var int
     *
     * @ORM\Column(name="savour", type="integer")
     */
    private $savour;
    /**
     * @var int
     *
     * @ORM\Column(name="supplement", type="integer")
     */
    private $supplement;
    /**
     * @var int
     *
     * @ORM\Column(name="topping", type="integer")
     */
    private $topping;
    /**
     * @var int
     *
     * @ORM\Column(name="colour", type="integer")
     */
    private $colour;
    /**
     * @var int
     *
     * @ORM\Column(name="candle", type="integer")
     */
    private $candle;
    /**
     * @var int
     *
     * @ORM\Column(name="text", type="integer")
     */
    private $text;
    /**
     * @var int
     *
     * @ORM\Column(name="active", type="integer")
     */
    private $active;

    /**
     * @var int
     *
     * @ORM\Column(name="isDel", type="integer", nullable=false)
     */
    private $isDel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $allergy;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $categorie;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creationDate;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }


    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getIngredient(): string
    {
        return $this->ingredient;
    }

    /**
     * @param string $ingredient
     */
    public function setIngredient(string $ingredient): void
    {
        $this->ingredient = $ingredient;
    }

    /**
     * @return array
     */
    public function getUrlImage(): ?array
    {
        return $this->urlImage;
    }

    /**
     * @param string $urlImage
     */
    public function setUrlImage(array $urlImage): void
    {
        $this->urlImage = $urlImage;
    }

    /**
     * @return int
     */
    public function getisDel(): int
    {
        return $this->isDel;
    }

    /**
     * @param int $isDel
     */
    public function setIsDel(int $isDel): void
    {
        $this->isDel = $isDel;
    }

    /**
     * @return mixed
     */
    public function getSpeciality()
    {
        return $this->speciality;
    }

    /**
     * @param mixed $speciality
     */
    public function setSpeciality($speciality): void
    {
        $this->speciality = $speciality;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     */
    public function setDuration(int $duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return string
     */
    public function getDayWeek()
    {
        return $this->dayWeek;
    }

    /**
     * @param string $dayWeek
     */
    public function setDayWeek(string $dayWeek): void
    {
        $this->dayWeek = $dayWeek;
    }

    /**
     * @return string
     */
    public function getInfoEtage()
    {
        return $this->infoEtage;
    }

    /**
     * @param string $infoEtage
     */
    public function setInfoEtage(string $infoEtage): void
    {
        $this->infoEtage = $infoEtage;
    }

    /**
     * @return int
     */
    public function getDummyStage(): int
    {
        return $this->dummyStage;
    }

    /**
     * @param int $dummyStage
     */
    public function setDummyStage(int $dummyStage): void
    {
        $this->dummyStage = $dummyStage;
    }

    /**
     * @return int
     */
    public function getSavour(): int
    {
        return $this->savour;
    }

    /**
     * @param int $savour
     */
    public function setSavour(int $savour): void
    {
        $this->savour = $savour;
    }

    /**
     * @return int
     */
    public function getSupplement(): int
    {
        return $this->supplement;
    }

    /**
     * @param int $supplement
     */
    public function setSupplement(int $supplement): void
    {
        $this->supplement = $supplement;
    }

    /**
     * @return int
     */
    public function getTopping(): int
    {
        return $this->topping;
    }

    /**
     * @param int $topping
     */
    public function setTopping(int $topping): void
    {
        $this->topping = $topping;
    }

    /**
     * @return int
     */
    public function getColour(): int
    {
        return $this->colour;
    }

    /**
     * @param int $colour
     */
    public function setColour(int $colour): void
    {
        $this->colour = $colour;
    }

    /**
     * @return int
     */
    public function getCandle(): int
    {
        return $this->candle;
    }

    /**
     * @param int $candle
     */
    public function setCandle(int $candle): void
    {
        $this->candle = $candle;
    }

    /**
     * @return int
     */
    public function getText(): int
    {
        return $this->text;
    }

    /**
     * @param int $text
     */
    public function setText(int $text): void
    {
        $this->text = $text;
    }



    /**
     * @return int
     */
    public function getActive(): int
    {
        return $this->active;
    }

    /**
     * @param int $active
     */
    public function setActive(int $active): void
    {
        $this->active = $active;
    }

    /**
     * @return int
     */
    public function getTypePatisserie(): int
    {
        return $this->typePatisserie;
    }

    /**
     * @param int $typePatisserie
     */
    public function setTypePatisserie(int $typePatisserie): void
    {
        $this->typePatisserie = $typePatisserie;
    }

    /**
     * @return int
     */
    public function getTypePrix(): int
    {
        return $this->typePrix;
    }

    /**
     * @param int $typePrix
     */
    public function setTypePrix(int $typePrix): void
    {
        $this->typePrix = $typePrix;
    }

    public function getAllergy(): ?string
    {
        return $this->allergy;
    }

    public function setAllergy(?string $allergy): self
    {
        $this->allergy = $allergy;

        return $this;
    }

    public function getCategorie(): ?string
    {
        return $this->categorie;
    }

    public function setCategorie(?string $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

}
