<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cake
 * @ORM\Table(name="speciality_list")
 * @ORM\Entity
 */
class SpecialityList
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */

    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255, nullable=false)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PastryChefSpecialityList", mappedBy="speciality")
     */
    private $specialities;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLibelle(): string
    {
        return $this->libelle;
    }

    /**
     * @param string $libelle
     */
    public function setLibelle(string $libelle): void
    {
        $this->libelle = $libelle;
    }

    /**
     * @return mixed
     */
    public function getSpecialities()
    {
        return $this->specialities;
    }

    /**
     * @param mixed $specialities
     */
    public function setSpecialities($specialities): void
    {
        $this->specialities = $specialities;
    }



}
