<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cake
 *
 * @ORM\Table(name="candle_option")
 * @ORM\Entity
 */
class CandleOption
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cake")
     * @ORM\JoinColumn(name="id_cake", referencedColumnName="id")
     */
    private $cake;

    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="float", length=11, nullable=false)
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="max", type="integer", length=11, nullable=false)
     */
    private $max;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCake()
    {
        return $this->cake;
    }

    /**
     * @param mixed $cake
     */
    public function setCake($cake): void
    {
        $this->cake = $cake;
    }

    /**
     * @return int
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getMax(): int
    {
        return $this->max;
    }

    /**
     * @param int $max
     */
    public function setMax(int $max): void
    {
        $this->max = $max;
    }




}
