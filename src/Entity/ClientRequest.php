<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRequestRepository")
 */
class ClientRequest
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="ClientRequests")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $location;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $coordinnates;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $event;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $numberPerson;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $informations;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creationDate;

    /**
     * @ORM\Column(type="text")
     */
    private $photo;

    /**
     * @ORM\Column(type="boolean")
     */
    private $accept;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ClientRequestMessage", mappedBy="ClientRequest")
     */
    private $clientRequestMessages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ClientRequestProposal", mappedBy="clientRequest")
     */
    private $clientRequestProposals;

    public function __construct()
    {
        $this->clientRequestMessages = new ArrayCollection();
        $this->clientRequestProposals = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getCoordinnates(): ?string
    {
        return $this->coordinnates;
    }

    public function setCoordinnates(string $coordinnates): self
    {
        $this->coordinnates = $coordinnates;

        return $this;
    }

    public function getEvent(): ?string
    {
        return $this->event;
    }

    public function setEvent(string $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getNumberPerson(): ?string
    {
        return $this->numberPerson;
    }

    public function setNumberPerson(string $numberPerson): self
    {
        $this->numberPerson = $numberPerson;

        return $this;
    }

    public function getInformations(): ?string
    {
        return $this->informations;
    }

    public function setInformations(string $informations): self
    {
        $this->informations = $informations;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getAccept()
    {
        return $this->accept;
    }

    public function setAccept($accept): void
    {
        $this->accept = $accept;
    }



    /**
     * @return Collection|ClientRequestMessage[]
     */
    public function getClientRequestMessages(): Collection
    {
        return $this->clientRequestMessages;
    }

    public function addClientRequestMessage(ClientRequestMessage $clientRequestMessage): self
    {
        if (!$this->clientRequestMessages->contains($clientRequestMessage)) {
            $this->clientRequestMessages[] = $clientRequestMessage;
            $clientRequestMessage->setClientRequest($this);
        }

        return $this;
    }

    public function removeClientRequestMessage(ClientRequestMessage $clientRequestMessage): self
    {
        if ($this->clientRequestMessages->contains($clientRequestMessage)) {
            $this->clientRequestMessages->removeElement($clientRequestMessage);
            // set the owning side to null (unless already changed)
            if ($clientRequestMessage->getClientRequest() === $this) {
                $clientRequestMessage->setClientRequest(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ClientRequestProposal[]
     */
    public function getClientRequestProposals(): Collection
    {
        return $this->clientRequestProposals;
    }

    public function addClientRequestProposal(ClientRequestProposal $clientRequestProposal): self
    {
        if (!$this->clientRequestProposals->contains($clientRequestProposal)) {
            $this->clientRequestProposals[] = $clientRequestProposal;
            $clientRequestProposal->setClientRequest($this);
        }

        return $this;
    }

    public function removeClientRequestProposal(ClientRequestProposal $clientRequestProposal): self
    {
        if ($this->clientRequestProposals->contains($clientRequestProposal)) {
            $this->clientRequestProposals->removeElement($clientRequestProposal);
            // set the owning side to null (unless already changed)
            if ($clientRequestProposal->getClientRequest() === $this) {
                $clientRequestProposal->setClientRequest(null);
            }
        }

        return $this;
    }
}
