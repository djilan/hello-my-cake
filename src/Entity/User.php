<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", nullable=false)
     */
    private $type;
    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=false)
     */
    private $username;
    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", nullable=false)
     */
    private $password;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;
    /**
     * @var string
     *
     * @ORM\Column(name="raisonSociale", type="string", length=255, nullable=false)
     */
    private $raisonSociale;
    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255, nullable=false)
     */
    private $firstName;
    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;
    /**
     * @var string
     *
     * @ORM\Column(name="address2", type="string", length=255, nullable=true)
     */
    private $address2;
    /**
     * @var string
     *
     * @ORM\Column(name="localisation", type="string", length=255, nullable=false)
     */
    private $localisation;
    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;
    /**
     * @var string
     *
     * @ORM\Column(name="zipCode", type="string", length=255, nullable=true)
     */
    private $zipCode;
    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;
    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=30, nullable=true)
     */
    private $phone;
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;
    /**
     * @var array
     *
     * @ORM\Column(name="picture", type="string", nullable=false)
     */
    private $picture;
    /**
     * @var array
     *
     * @ORM\Column(name="days", type="array", length=400, nullable=false)
     */
    private $days;
    /**
    /**
     * @var array
     *
     * @ORM\Column(name="aRoles", type="array", length=400, nullable=false)
     */
    private $aroles;
    /**
     * @var int
     *
     * @ORM\Column(name="active", type="integer", nullable=false)
     */
    private $active;
    /**
     * @var int
     *
     * @ORM\Column(name="isDel", type="integer", nullable=false)
     */
    private $isDel;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PastryChefSpecialityList", mappedBy="user")
     */
    private $specialities;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ClientRequest", mappedBy="user", orphanRemoval=true)
     */
    private $ClientRequests;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ClientRequestProposal", mappedBy="user")
     */
    private $clientRequestProposals;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ClientRequestProposal", mappedBy="userPastry")
     */
    private $clientRequestProposalPastry;

    public function __construct()
    {
        $this->ClientRequests = new ArrayCollection();
        $this->clientRequestProposals = new ArrayCollection();
        $this->clientRequestProposalPastry = new ArrayCollection();
    }

    //GETTER & SETTER

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getRaisonSociale()
    {
        return $this->raisonSociale;
    }

    /**
     * @param string $raisonSociale
     */
    public function setRaisonSociale(string $raisonSociale): void
    {
        $this->raisonSociale = $raisonSociale;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * @param string $address2
     */
    public function setAddress2(string $address2): void
    {
        $this->address2 = $address2;
    }

    /**
     * @return string
     */
    public function getLocalisation()
    {
        return $this->localisation;
    }

    /**
     * @param string $localisation
     */
    public function setLocalisation(string $localisation): void
    {
        $this->localisation = $localisation;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     */
    public function setZipCode(string $zipCode): void
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country): void
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getRoles(): ?array
    {
        return $this->aroles;
    }

    public function setRoles(array $aroles): self
    {
        $this->aroles = $aroles;

        return $this;
    }

    /**
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param array $picture
     */
    public function setPicture(string $picture): void
    {
        $this->picture = $picture;
    }

    public function getIsDel(): ?int
    {
        return $this->isDel;
    }

    public function setIsDel(int $isDel): self
    {
        $this->isDel = $isDel;

        return $this;
    }

    /**
     * @return int
     */
    public function getActive(): int
    {
        return $this->active;
    }

    /**
     * @param int $active
     */
    public function setActive(int $active): void
    {
        $this->active = $active;
    }



    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return mixed
     */
    public function getSpecialities()
    {
        return $this->specialities;
    }

    /**
     * @param mixed $specialities
     */
    public function setSpecialities($specialities): void
    {
        $this->specialities = $specialities;
    }

    /**
     * @return array
     */
    public function getDays(): array
    {
        return $this->days;
    }

    /**
     * @param array $days
     */
    public function setDays(array $days): void
    {
        $this->days = $days;
    }

    /**
     * @return Collection|ClientRequest[]
     */
    public function getClientRequests(): Collection
    {
        return $this->ClientRequests;
    }

    public function addClientRequest(ClientRequest $ClientRequest): self
    {
        if (!$this->ClientRequests->contains($ClientRequest)) {
            $this->ClientRequests[] = $ClientRequest;
            $ClientRequest->setUser($this);
        }

        return $this;
    }

    public function removeClientRequest(ClientRequest $ClientRequest): self
    {
        if ($this->ClientRequests->contains($ClientRequest)) {
            $this->ClientRequests->removeElement($ClientRequest);
            // set the owning side to null (unless already changed)
            if ($ClientRequest->getUser() === $this) {
                $ClientRequest->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ClientRequestProposal[]
     */
    public function getClientRequestProposals(): Collection
    {
        return $this->clientRequestProposals;
    }

    public function addClientRequestProposal(ClientRequestProposal $clientRequestProposal): self
    {
        if (!$this->clientRequestProposals->contains($clientRequestProposal)) {
            $this->clientRequestProposals[] = $clientRequestProposal;
            $clientRequestProposal->setUser($this);
        }

        return $this;
    }

    public function removeClientRequestProposal(ClientRequestProposal $clientRequestProposal): self
    {
        if ($this->clientRequestProposals->contains($clientRequestProposal)) {
            $this->clientRequestProposals->removeElement($clientRequestProposal);
            // set the owning side to null (unless already changed)
            if ($clientRequestProposal->getUser() === $this) {
                $clientRequestProposal->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ClientRequestProposal[]
     */
    public function getClientRequestProposalPastry(): Collection
    {
        return $this->clientRequestProposalPastry;
    }

    public function addClientRequestProposalPastry(ClientRequestProposal $clientRequestProposalPastry): self
    {
        if (!$this->clientRequestProposalPastry->contains($clientRequestProposalPastry)) {
            $this->clientRequestProposalPastry[] = $clientRequestProposalPastry;
            $clientRequestProposalPastry->setUserPastry($this);
        }

        return $this;
    }

    public function removeClientRequestProposalPastry(ClientRequestProposal $clientRequestProposalPastry): self
    {
        if ($this->clientRequestProposalPastry->contains($clientRequestProposalPastry)) {
            $this->clientRequestProposalPastry->removeElement($clientRequestProposalPastry);
            // set the owning side to null (unless already changed)
            if ($clientRequestProposalPastry->getUserPastry() === $this) {
                $clientRequestProposalPastry->setUserPastry(null);
            }
        }

        return $this;
    }





}
