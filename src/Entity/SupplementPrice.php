<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cake
 *
 * @ORM\Table(name="supplement_price")
 * @ORM\Entity
 */
class SupplementPrice
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SupplementList")
     * @ORM\JoinColumn(name="id_supplement", referencedColumnName="id")
     */
    private $supplement;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cake")
     * @ORM\JoinColumn(name="id_cake", referencedColumnName="id")
     */
    private $cake;

    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="float", length=11, nullable=false)
     */
    private $price;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getSupplement()
    {
        return $this->supplement;
    }

    /**
     * @param mixed $supplement
     */
    public function setSupplement($supplement): void
    {
        $this->supplement = $supplement;
    }

    /**
     * @return mixed
     */
    public function getCake()
    {
        return $this->cake;
    }

    /**
     * @param mixed $cake
     */
    public function setCake($cake): void
    {
        $this->cake = $cake;
    }

    /**
     * @return int
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }
    
}
