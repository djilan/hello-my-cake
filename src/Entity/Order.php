<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cake
 *
 * @ORM\Table(name="orders")
 * @ORM\Entity
 */
class Order
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OrderCake", mappedBy="order")
     */
    private $orderCake;

    /**
     * @var integer
     *
     * @ORM\Column(name="code", type="integer", length=6, nullable=false)
     */
    private $code;

    /**
     * @var integer
     *
     * @ORM\Column(name="validate", type="integer", length=6, nullable=false)
     */
    private $validate;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_creation", type="date", nullable=false)
     */
    private $date_creation;

    /**
     * @var string
     *
     * @ORM\Column(name="info", type="string", length=500)
     */
    private $info;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getOrderCake()
    {
        return $this->orderCake;
    }

    /**
     * @param mixed $cakes
     */
    public function setOrderCake($orderCake): void
    {
        $this->orderCake = $orderCake;
    }

    /**
     * @return int
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * @param int $date_creation
     */
    public function setDateCreation($date_creation): void
    {
        $this->date_creation = $date_creation;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode(int $code): void
    {
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getValidate(): int
    {
        return $this->validate;
    }

    /**
     * @param int $validate
     */
    public function setValidate(int $validate): void
    {
        $this->validate = $validate;
    }

    /**
     * @return string
     */
    public function getInfo(): string
    {
        return $this->info;
    }

    /**
     * @param string $info
     */
    public function setInfo(string $info): void
    {
        $this->info = $info;
    }


}
