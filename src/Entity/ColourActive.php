<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cake
 *
 * @ORM\Table(name="colour_active")
 * @ORM\Entity
 */
class ColourActive
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cake")
     * @ORM\JoinColumn(name="id_cake", referencedColumnName="id")
     */
    private $cake;

    /**
     * @var string
     *
     * @ORM\Column(name="colour_list", type="string", length=255, nullable=false)
     */
    private $colourList;
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCake()
    {
        return $this->cake;
    }

    /**
     * @param mixed $cake
     */
    public function setCake($cake): void
    {
        $this->cake = $cake;
    }

    /**
     * @return string
     */
    public function getColourList(): string
    {
        return $this->colourList;
    }

    /**
     * @param string $colourList
     */
    public function setColourList(string $colourList): void
    {
        $this->colourList = $colourList;
    }

}
