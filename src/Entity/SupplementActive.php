<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cake
 *
 * @ORM\Table(name="supplement_active")
 * @ORM\Entity
 */
class SupplementActive
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cake")
     * @ORM\JoinColumn(name="id_cake", referencedColumnName="id")
     */
    private $cake;

    /**
     * @var string
     *
     * @ORM\Column(name="supplement_list", type="string", length=255, nullable=false)
     */
    private $supplementList;
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCake()
    {
        return $this->cake;
    }

    /**
     * @param mixed $cake
     */
    public function setCake($cake): void
    {
        $this->cake = $cake;
    }

    /**
     * @return string
     */
    public function getSupplementList(): string
    {
        return $this->supplementList;
    }

    /**
     * @param string $supplementList
     */
    public function setSupplementList(string $supplementList): void
    {
        $this->supplementList = $supplementList;
    }

}
