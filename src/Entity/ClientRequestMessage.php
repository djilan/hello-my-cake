<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRequestMessageRepository")
 */
class ClientRequestMessage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ClientRequest", inversedBy="clientRequestMessages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $clientRequest;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\user")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\user")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userPastry;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creationDate;

    /**
     * @ORM\Column(type="string", length=3000, nullable=true)
     */
    private $message;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $photo;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $sender;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClientRequest(): ?ClientRequest
    {
        return $this->clientRequest;
    }

    public function setClientRequest(?ClientRequest $clientRequest): self
    {
        $this->clientRequest = $clientRequest;

        return $this;
    }

    public function getUser(): ?user
    {
        return $this->user;
    }

    public function setUser(?user $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUserPastry(): ?user
    {
        return $this->userPastry;
    }

    public function setUserPastry(?user $userPastry): self
    {
        $this->userPastry = $userPastry;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getSender(): ?string
    {
        return $this->sender;
    }

    public function setSender(string $sender): self
    {
        $this->sender = $sender;

        return $this;
    }


}
