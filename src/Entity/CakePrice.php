<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cake
 * @ORM\Table(name="price_cake")
 * @ORM\Entity
 */
class CakePrice
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */

    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cake")
     * @ORM\JoinColumn(name="cake_id", referencedColumnName="id")
     */
    private $cake;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CakeSize")
     * @ORM\JoinColumn(name="size", referencedColumnName="id")
     */
    private $size;

    /**
     * @var integer
     * @ORM\Column(name="price", type="float", length=11, nullable=false)
     */
    private $price;

    /**
     * @var integer
     * @ORM\Column(name="quantity_min", type="integer", length=3, nullable=true)
     */
    private $quantityMin;

    /**
     * @ORM\Column(type="integer", nullable=true, length=3)
     */
    private $quantityMax;

    /**
     * @ORM\Column(type="integer", nullable=true, length=3)
     */
    private $quantity;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $oddEvenQuantity;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCake()
    {
        return $this->cake;
    }

    /**
     * @param mixed $cake
     */
    public function setCake($cake): void
    {
        $this->cake = $cake;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size): void
    {
        $this->size = $size;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getQuantityMin(): int
    {
        return $this->quantityMin;
    }

    /**
     * @param int $quantityMin
     */
    public function setQuantityMin(int $quantityMin): void
    {
        $this->quantityMin = $quantityMin;
    }

    public function getQuantityMax(): ?int
    {
        return $this->quantityMax;
    }

    public function setQuantityMax(?int $quantityMax): self
    {
        $this->quantityMax = $quantityMax;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getOddEvenQuantity(): ?bool
    {
        return $this->oddEvenQuantity;
    }

    public function setOddEvenQuantity(bool $oddEvenQuantity): self
    {
        $this->oddEvenQuantity = $oddEvenQuantity;

        return $this;
    }


}
