<?php
/**
 * Created by PhpStorm.
 * User: Djilan
 * Date: 22/10/2019
 * Time: 22:51
 */

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function getPastryChef()
    {
        $qb = $this->createQueryBuilder('u')
            ->innerJoin('u.specialities','s')
            ->where('u.aroles LIKE :roles')
            ->setParameter('roles', '%"ROLE_PASTRY_CHEF"%')
            ->andWhere('u.username != :username')
            ->setParameter('username', 'contact@hellomycake.com')
            ->andWhere('u.type = 1')
            ->andWhere('u.active = 1')
            ->andWhere('u.isDel = 0');

        $query = $qb->getQuery();

        return $query->getResult();
    }

    public function getOnePastryChef($id)
    {
        $qb = $this->createQueryBuilder('u')
            ->innerJoin('u.specialities','s')
            ->where('u.aroles LIKE :roles')
            ->setParameter('roles', '%"ROLE_PASTRY_CHEF"%')
            ->andWhere('u.id LIKE :id')
            ->setParameter('id', $id)
            ->andWhere('u.active = 1')
            ->andWhere('u.isDel = 0');

        $query = $qb->getQuery();

        return $query->getOneOrNullResult();
    }
}