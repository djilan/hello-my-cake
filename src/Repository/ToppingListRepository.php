<?php
/**
 * Created by PhpStorm.
 * User: Djilan
 * Date: 22/10/2019
 * Time: 22:51
 */

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class ToppingListRepository extends EntityRepository
{

    public function getToppingList($id)
    {
        $qb = $this->createQueryBuilder('sa')
            ->where('sa.user is null or sa.user = :iduser')
            ->setParameter('iduser', $id);

        $query = $qb->getQuery();

        return $query->getResult();
    }
}