var recap_tab = [];
$(function (){
    //Bouton etape suivante
    $('.next-button').click(function (e){
        e.preventDefault();
        var next_step = $(this).attr('next-step');
        var current_step = $(this).attr('current-step');
        // var title = $(this).parents('.modal-content').find('.modal-title').text();
        if($(this).hasClass('final-step')){
            var selects = $('select');
            selects.each(function (){
                var val_select = $(this).val();
                if(typeof val_select == 'object'){
                    var tmp_select = '';
                    for (let i = 0; i < val_select.length; i++) {
                        tmp_select += val_select[i]+','
                    }
                    val_select = tmp_select.slice(0, -1);
                    var data_name = $(this).attr('data-name');
                    $('input[name='+data_name+']').val(val_select);
                }
            })
            $('#add_cake_form').submit()
        }
        var inputs = $('.'+current_step.substr(1)+'-input');
        inputs.each(function (index){
            var required = $(this).attr('required');
            //Si le champ est obligatoire
            var title = $(this).prev().text();
            var tmp = [];

            //Si c'est un input
            if($(this).is('textarea') || ($(this).is('input') && ($(this).attr('type') == 'text' || $(this).attr('type') == 'number'))){
                if(typeof required !== 'undefined' && required !== false) {
                    var val_input = $(this).val();
                    if (val_input.trim() != '') {

                        $(next_step).modal('show');
                        $(current_step).modal('hide');
                        $(next_step).find('.previous-button').attr('previous-step', current_step);
                        var type = "text"
                        if(typeof $(this).attr('data-type') !== 'undefined'){
                            type = "price"
                        }
                        tmp.push({
                            "id": current_step.substr(1),
                            "title": title,
                            "type": type,
                            "value": val_input.trim()
                        });
                        recap_tab.push(tmp);
                        recap(current_step.substr(1) + index, tmp)
                        $('.' + current_step.substr(1) + '-erreur').hide()
                    }
                    else {
                        $('.' + current_step.substr(1) + '-erreur').show()
                    }
                }
                else{
                    var type = "text"
                    if(typeof $(this).attr('data-type') !== 'undefined'){
                        type = "price"
                    }
                    tmp.push({
                        "id": current_step.substr(1),
                        "title": title,
                        "type": type,
                        "value": val_input.trim()
                    });
                    recap_tab.push(tmp);
                    recap(current_step.substr(1) + index, tmp)
                    $(next_step).modal('show');
                    $(current_step).modal('hide');
                }
            }
            //Check pour un select ou un radio
            else if($(this).is('input') && $(this).attr('type') == 'radio'){
                if(typeof required !== 'undefined' && required !== false) {
                    var radios = inputs;
                    var checked_radio = false;
                    radios.each(function (){
                        if($(this).prop('checked')){
                            checked_radio = true;
                        }
                    });
                    if(checked_radio){
                        //etape suivant sélection prix
                        if($(this).parents('.'+current_step.substr(1)+'-border').hasClass('choice-management') && $(this).parents('.'+current_step.substr(1)+'-border').hasClass('choice-management') && $(this).prop('checked')){
                            next_step = $(this).parents('.'+current_step.substr(1)+'-border.choice-management.active').attr('data-target');
                        }
                        $(next_step).modal('show');
                        $(current_step).modal('hide');
                        $(next_step).find('.previous-button').attr('previous-step',current_step);
                        $('.'+current_step.substr(1)+'-erreur').hide()
                        if($(this).prop('checked')){
                            tmp.push({
                                "id": current_step.substr(1),
                                "title":title,
                                "type": "text",
                                "value":$(this).attr('input-val')
                            });
                            recap_tab.push(tmp);
                            recap(current_step.substr(1)+index,tmp)
                        }

                    }
                    else{
                        //Erreur radio
                        var parents_radio = $(this).parents('.'+current_step.substr(1)+'-border');
                        parents_radio.css('border','4px solid red');
                        $('.'+current_step.substr(1)+'-erreur').show();
                    }
                }
                else{
                    $(next_step).modal('show');
                    $(current_step).modal('hide');
                }
            }

            else if($(this).is('input') && $(this).attr('type') == 'file'){
                if(typeof required !== 'undefined' && required !== false) {
                    var val_input = $(this).val();
                    if (val_input.trim() != '') {
                        $(next_step).modal('show');
                        $(current_step).modal('hide');
                        $(next_step).find('.previous-button').attr('previous-step', current_step);
                        tmp.push({
                            "id": current_step.substr(1),
                            "title": title,
                            "type": "file"
                        });
                        recap_tab.push(tmp);
                        recap(current_step.substr(1) + index, tmp)
                    }else {
                        $('.' + current_step.substr(1) + '-erreur').show()
                    }
                }
                else{
                    tmp.push({
                        "id": current_step.substr(1),
                        "title": title,
                        "type": "file",
                    });
                    $(next_step).modal('show');
                    $(current_step).modal('hide');
                }
            }

            else if ($(this).is('select')){
                var val_input = $(this).val();
                title = $(this).attr('data-title');
                if(typeof required !== 'undefined' && required !== false) {
                    if(typeof val_input == 'object'){
                        var tmp_text = '';
                        for (let i = 0; i < val_input.length; i++) {
                            tmp_text += val_input[i]+','
                        }
                        val_input = tmp_text.slice(0, -1);
                    }
                    if (val_input.trim() != '') {
                        //recap(block, texts)
                        $(next_step).modal('show');
                        $(current_step).modal('hide');
                        $(next_step).find('.previous-button').attr('previous-step', current_step);
                        tmp.push({
                            "id": current_step.substr(1),
                            "title": title,
                            "type": "text",
                            "value": val_input.trim()
                        });
                        recap_tab.push(tmp);
                        recap(current_step.substr(1) + index, tmp)
                        $('.' + current_step.substr(1) + '-erreur').hide()
                    }
                    else {
                        $('.' + current_step.substr(1) + '-erreur').show()
                    }
                }
                else{
                    if(typeof val_input == 'object'){
                        var tmp_text = '';
                        for (let i = 0; i < val_input.length; i++) {
                            tmp_text += val_input[i]+','
                        }
                        val_input = tmp_text.slice(0, -1);
                        var value_form = $(this).attr('data-name');
                        $('input[name='+value_form+']').val(val_input);
                    }
                    tmp.push({
                        "id": current_step.substr(1),
                        "title": title,
                        "type": "text",
                        "value": val_input.trim()
                    });
                    recap_tab.push(tmp);
                    recap(current_step.substr(1) + index, tmp)

                    $(next_step).modal('show');
                    $(current_step).modal('hide');
                }
            }

        });
        if(inputs.length == 0){
            //Check si c'est pas une étape pour afficher un message
            if(!$(this).hasClass('step-message')){
                if($(this).hasClass('final-step')){
                    $('#add_cake_form').submit()
                }else{
                    $('.'+current_step.substr(1)+'-erreur').show()
                }
            }else{
                $(next_step).modal('show');
                $(current_step).modal('hide');
                $('.'+current_step.substr(1)+'-erreur').hide()
            }
        }
    });

    //Bouton etape précédente
    $('.previous-button').click(function (e){
        e.preventDefault();

        var previous_step = $(this).attr('previous-step');
        var current_step = $(this).attr('current-step');

        $(previous_step).modal('show');
        $(current_step).modal('hide');

        var recap_tab_new = [];

        for (let i = 0; i < recap_tab.length; i++) {
            if('#'+recap_tab[i][0].id != previous_step){
                recap_tab_new.push(recap_tab[i]);
            }else{
                $('p[id^=block-recap-'+recap_tab[i][0].id+']').remove();
            }
        }

        recap_tab = recap_tab_new;
    });

    //Gros gateau ou petit gateau
    $('.type-gateau-modal').click(function (){
        $('.type-gateau-modal').removeClass('active');
        $('.type-gateau-modal').css('background-color', 'white');
        $('.type-gateau-modal').css('border','4px solid #5CB1C0');
        $(this).addClass('active');
        $(this).css('background-color', '#ffe7ee');
        $(this).find('input').prop('checked', true);
    })

    $('.type-gateau-modal').each(function (){
        if($(this).find('input').prop('checked')){
            $(this).addClass('active');
            $(this).css('background-color', '#ffe7ee');
        }
    })

    function recap(block_id, texts){

        var block_recap = $('#block-recap-'+block_id);
        block_recap.remove();
        $('#modal-cake-recap .modal-body-presentation').append('<p id="block-recap-'+block_id+'"></p>');
        var block_recap_new = $('#block-recap-'+block_id);

        for (let i = 0; i < texts.length; i++) {
            if(texts[i].type == "text"){
                var text_value = 'Non renseigné';
                if(texts[i].value != '') {
                    text_value = texts[i].value;
                }
                block_recap_new.append('' +
                    '<p><strong>'+texts[i].title+'</strong> '+text_value+'</p>'
                )
            }
            if(texts[i].type == "price"){
                block_recap_new.append('' +
                    '<p><strong>'+texts[i].title+'</strong> '+texts[i].value+'€</p>'
                )
            }
            if(texts[i].type == "file"){
                var src = $('.image-preview').attr('src')
                block_recap_new.append('' +
                    '<p><img class="image-preview" width="" src="'+src+'" alt=""></p>'
                )
            }
        }
    }

    //SELECT ALLERGY - CATEGORIE
    var tab = [$('select[multiple].select1'), $('select[multiple].select2')];

    for (const select of tab) {
        var options = select.find('option');

        var div = $('<div />').addClass('selectMultiple');
        var active = $('<div />');
        var list = $('<ul />');
        var placeholder = select.data('placeholder');

        var span = $('<span />').text(placeholder).appendTo(active);

        options.each(function() {
            var text = $(this).text();
            if($(this).is(':selected')) {
                active.append($('<a />').html('<em>' + text + '</em><i></i>'));
                span.addClass('hide');
            } else {
                list.append($('<li />').html(text));
            }
        });

        active.append($('<div />').addClass('arrow'));
        div.append(active).append(list);

        select.wrap(div);
    }

    $(document).on('click', '.selectMultiple ul li', function(e) {
        var select = $(this).parent().parent();
        var li = $(this);
        if(!select.hasClass('clicked')) {
            select.addClass('clicked');
            li.prev().addClass('beforeRemove');
            li.next().addClass('afterRemove');
            li.addClass('remove');
            var a = $('<a />').addClass('notShown').html('<em>' + li.text() + '</em><i></i>').hide().appendTo(select.children('div'));
            a.slideDown(100, function() {
                setTimeout(function() {
                    a.addClass('shown');
                    select.children('div').children('span').addClass('hide');
                    select.find('option:contains(' + li.text() + ')').prop('selected', true);
                }, 100);
            });
            setTimeout(function() {
                if(li.prev().is(':last-child')) {
                    li.prev().removeClass('beforeRemove');
                }
                if(li.next().is(':first-child')) {
                    li.next().removeClass('afterRemove');
                }
                setTimeout(function() {
                    li.prev().removeClass('beforeRemove');
                    li.next().removeClass('afterRemove');
                }, 100);

                li.slideUp(100, function() {
                    li.remove();
                    select.removeClass('clicked');
                });
            }, 100);
        }
    });

    $(document).on('click', '.selectMultiple > div a', function(e) {
        var select = $(this).parent().parent();
        var self = $(this);
        self.removeClass().addClass('remove');
        select.addClass('open');
        setTimeout(function() {
            self.addClass('disappear');
            setTimeout(function() {
                self.animate({
                    width: 0,
                    height: 0,
                    padding: 0,
                    margin: 0
                }, 100, function() {
                    var li = $('<li />').text(self.children('em').text()).addClass('notShown').appendTo(select.find('ul'));
                    li.slideDown(400, function() {
                        li.addClass('show');
                        setTimeout(function() {
                            select.find('option:contains(' + self.children('em').text() + ')').prop('selected', false);
                            if(!select.find('option:selected').length) {
                                select.children('div').children('span').removeClass('hide');
                            }
                            li.removeClass();
                        }, 100);
                    });
                    self.remove();
                })
            }, 100);
        }, 100);
    });

    $(document).on('click', '.selectMultiple > div .arrow, .selectMultiple > div span', function(e) {
        $(this).parent().parent().toggleClass('open');
    });

})
